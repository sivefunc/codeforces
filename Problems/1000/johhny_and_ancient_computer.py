##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Johnny and Ancient Computer                              #
# Link: https://codeforces.com/contest/1362/submission/221852751 #
# Contest: 1362                                                  #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 108 ms                                                   #
# Memory: 4076 KB                                                #
##################################################################

def min_ops(a: int, b: int) -> int:
    # n = log2(b/a if a <= b else a/b) this is not precise :/
    a, b = sorted((a, b))
    ops = 0
    while a < b:
        a *= 2
        ops += 1
    
    return (ops+2)//3 if a==b else -1
    
for t_case in range(int(input())):
    a, b = map(int, input().split())
    print(min_ops(a, b))
