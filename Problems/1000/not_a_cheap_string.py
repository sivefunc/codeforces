##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Not a Cheap String                                       #
# Link: https://codeforces.com/contest/1702/submission/206033926 #
# Contest: 1702                                                  #
# Problem: D                                                     #
# Rating: 1000                                                   #
# Status: Unsolved                                               #
# Time: 2000+ ms                                                 #
# Memory: 4500 KB                                                #
##################################################################

# Explanation
#
# To get the minimal number of ch removal, we just start removing
# the characters that have a higher price, for each removal we check
# if the total price of string is <= p
#
# e.g: 'af' with p=1, instead of removing a, we remove the letter
# f because it has a higher individual price.

# I dont understand why my solution takes 2+ seconds in test case 3
# After doing my solution, I try to see other people solution and some people
# is doing the things like me, so i couldn't understand.
# Even the solution that made this user:
# https://codeforces.com/contest/1702/submission/166295288
# Is faster than mine, and is almost identical (he uses while loops)
#
# I tried another methods of replacing the str.replace but none of these worked
# I'm going to keep to solve another problems, and come back to this prob in 1
# month?

# Implementation
def min_chs_rm(w: str, p: int) -> str:

    # Ord gives u the ascii value, substract 96 because we are counting from 1
    # This method of getting values only works with original ascii 127 values.
    # So if u are looking to use another alphabet, I think u have to create
    # a dict with the values.

    if (total_price := sum(ord(ch) - 96 for ch in w)) <= p:
        return w

    for ch in ''.join(sorted(w, reverse=True)): # sort from high to lower

        # remove that high price character and check
        if (total_price := total_price - (ord(ch) - 96)) <= p:
            return w.replace(ch, '', 1) # is this making it slower?

        else:
            w = w.replace(ch, '', 1)

# Program running
for t_case in range(int(input())):
    w = input()
    p = int(input())
    print(min_chs_rm(w, p))
