##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Exchange                                                 #
# Link: https://codeforces.com/contest/1765/submission/205571666 #
# Contest: 1765                                                  #
# Problem: E                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 109 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# n, a, b = Price of weapon, gold to silver, silver to gold
#
# If exchange of gold to silver is greater than silver to gold
# it means that infinite money exploit exists
#
# In the case it doesn't exist, just divide the weapon by gold to silver
# and add 1 if it's a float, because division represents the number of quests

# Implementation
for t_case in range(int(input())):
    n, a, b = map(int, input().split())
    quests = 1 if b < a else (n // a + 1 if n % a else n // a)
    print(quests)


