##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: New theatre square                                       #
# Link: https://codeforces.com/contest/1359/submission/221712795 #
# Contest: 1359                                                  #
# Problem: B                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 421 ms                                                   #
# Memory: 9664 KB                                                #
##################################################################

for t_case in range(int(input())):
    n, m, x, y = map(int, input().split())
    total_price = 0
    for row in range(n):
        for sq in input().replace('*', ' ').split():
            if y < 2*x:
                total_price += len(sq)//2 * y + len(sq)%2 * x
            
            else:
                total_price += len(sq)*x

    print(total_price)
