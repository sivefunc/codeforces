##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: 2048 Game                                                #
# Link: https://codeforces.com/contest/1221/submission/205908586 #
# Contest: 1221                                                  #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 176 KB                                                   #
##################################################################

# Explanation
#
# A number is contructed from others
# 2048 is constructed from 1024 * 2
# 1024 is constructed from 512 * 2
# 512 is constructed from 256 * 2 and so on.
# n is constructed from n/2 + n/2 the same as (n / 2) * 2
#
#               2048
#              /    \
#          1024      1024
#         /    \    /    \
#        512   512 512   512
#       /   \ 
#     256  256
#    /
#  128
#   /
#  64
#
# We want to know if from a list of number you can construct a number
#
# To know that, we just start dividing n / 2 and find if that number
# is on the number list (at least 2 elements needed)

# Implementation
from math import log2 # to get powers of 2

def is_win_possible(numbers: list, win=2048) -> bool:
    
    m = 1 # Number of tiles needed to construct an upper number

    # 2**n, going from win tile to 2**0
    tiles = [2 ** tile for tile in range(int(log2(win)), -1, -1)]
    
    for tile in tiles: # gen the number graph from win tile up to 1.

        if tile not in numbers: # Number on graph not found
            # multiply 2 because that number can be constructed from (n/2) * 2
            m *= 2

        elif numbers.count(tile) >= m:
            # if there are enough tiles to construct the upper number it means
            # that the win tile can be constructed
            return True

        else:
            # (substract the tiles u found - m) and these are the tiles left to
            # construct the upper number, multiply the result by two because
            # these tiles can be constructed from n / 2 + n / 2
            m = (m - numbers.count(tile)) * 2
    
    return False

# Program running
for query in range(int(input())):
    n = input() # len of numbers
    numbers = [int(number) for number in input().split()]
    result = 'yes' if is_win_possible(numbers) else 'no'
    
    print(result)
