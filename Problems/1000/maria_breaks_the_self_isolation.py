##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Maria breaks the self isolation                          #
# Link: https://codeforces.com/contest/1358/submission/221472726 #
# Contest: 1358                                                  #
# Problem: B                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 312 ms                                                   #
# Memory: 13240 KB                                               #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    grannies = list(map(int, input().split()))
    total = 1
    for idx, i in enumerate(sorted(grannies, reverse=True)):
        if n - idx >= i:
            total += n - idx
            break

    print(total)
