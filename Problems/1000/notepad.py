##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Notepad#                                                 #
# Link: https://codeforces.com/contest/1766/submission/206966213 #
# Contest: 1766                                                  #
# Problem: B                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 155 ms                                                   #
# Memory: 600 KB                                                 #
##################################################################

# Explanation
#
# To know if it's possible
# We need a subsequence of 2 characters that get repeated in the sequence
# Because you can copy that subsequence to make the string
# and reduce the number of operations being n-1 < n

for t_case in range(int(input())):
    n = int(input())
    s = input()

    if n > 2:
        for idx, ch in enumerate(s[:-2]):
            if ch+s[idx+1] in s[idx+2:]:
                result = 'YES'
                break
            result = 'NO'
    
    else:
        result = 'NO'
    print(result)
