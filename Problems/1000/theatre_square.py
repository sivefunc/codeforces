##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Theatre Square                                           #
# Link: https://codeforces.com/contest/1/submission/221717887    #
# Contest: 1                                                     #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 62 ms                                                    #
# Memory: 16 KB                                                  #
##################################################################

n, m, a = map(int, input().split())
 
if a >= m and a >= n:
    result = 1

elif a >= m:
    result = n//a + n%a

elif a >= n:
    result = m//a + m%a

else:
    result = (n//a + (1 if n%a else 0)) * (m//a + (1 if m % a else 0))

print(result)
