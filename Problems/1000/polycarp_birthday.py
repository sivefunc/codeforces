##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Happy Birthday, Polycarp!                                #
# Link: https://codeforces.com/contest/1277/submission/205778837 #
# Contest: 1277                                                  #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 155 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# beautiful years, is eq. to 9 * len of n
# 1 ... 9           -> +9  
# 11 ... 99         -> +9
# 111 .... 999      -> +9
# 1111 ..... 9999   -> +9
# 11111 ..... 99999 -> +9
#
# Substract some years, for each number that is greater than n
# in the range (1, 9)
#
# e.g: 5555
# years = 9*4 and substract 4 because, 9999,8888,7777 and 6666 are > than 5555

# Implementation a little different.
def n_of_byears(n: str) -> int:
    n_of_byears = 9 * (len(n) - 1)
    years = [1 for year in range(1, 10) if int(n) >= int(str(year) * len(n))]
    
    return n_of_byears + years.count(1)

for t_case in range(int(input())):
    n = input() # How many years has turned
    b_years = n_of_byears(n)
    print(b_years)
