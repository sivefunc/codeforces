##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Asterik-Minor Template                                   #
# Link: https://codeforces.com/contest/1796/submission/207075371 #
# Contest: 1796                                                  #
# Problem: B                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 187 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Find a subsequence that appears in 'a' and 'b' and put asteriks on the sides
# of subsequence.
#
# The subsequence needs to be at least 2 characters.
#
# This is done because you need asteriks being <= latin characters in template.
# 
# There are some edge cases if you can't find subsequence >= 2:
#
# 1) First of 'a' and b are equal, in that case we can add an asterik to right
# side of that ch because, asterik <= ch.
#
# 2) Last of 'a' and b are equal, in this case we can add an asterik to left
# side of that ch because, asterik <= ch.

def find_t(long: str, short: str) -> str:
     
    for idx, ch in enumerate(long[:-1]):
        if ch + long[idx+1] in short:
            return '*' + ch + long[idx+1] + '*'
    return ''

for t_case in range(int(input())):
    a = input()
    b = input()

    # imaging doing this one liner with ternary operators
    if a == b: t = a # using FIND_T also works, this is just faster.
   
    elif a[0] == b[0]: t = a[0] + '*' # edge case n1
    elif a[-1] == b[-1]: t = '*' + a[-1] # edge case n2

    else: t = find_t(a, b)

    result = f'YES\n{t}' if t else 'NO'
    print(result)
