##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Digits Sequence (Easy Edition)                           #
# Link: https://codeforces.com/contest/1177/submission/205492680 #
# Contest: 1177                                                  #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 476 KB                                                 #
##################################################################

# Explanation
#
# Just generate the list of numbers and find the number in the given index

# Implementation

k = int(input()) # position of digit
sequence = ''.join(str(n) for n in range(1, k + 1))
digit = sequence[k - 1]
print(digit)
