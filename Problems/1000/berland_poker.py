##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Berland Poker                                            #
# Link: https://codeforces.com/contest/1359/submission/221591802 #
# Contest: 1359                                                  #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 77 ms                                                    #
# Memory: 1780 KB                                                #
##################################################################

for t_case in range(int(input())):

    n, m, k = map(int, input().split())
    if n//k >= m:
        print(m)

    elif k-1 > m - n//k:
        print(n//k - 1)

    else:
        y = (m - n//k) // (k - 1)
        print(n // k - (y + (1 if (m - n//k) % (k-1) else 0)))
