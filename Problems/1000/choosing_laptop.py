##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Choosing Laptop                                          #
# Link: https://codeforces.com/contest/106/submission/205492279  #
# Contest: 106                                                   #
# Problem: B                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# to find the updated laptops, just go through each laptop and compare it
# to each laptop in the market, if specs are strictly less remove it.
#
# at the end u will have a list of laptops updated
# just choose the cheapest

# codeforces doesn't accept this
#og_laptops = [[int(data) for data in laptop.split()] for laptop in \
#        open('input.txt').readlines()[1:]]

# Implementation
og_laptops = [[int(data) for data in input().split()] for n in \
        range(int(input()))]

updated_laptops = og_laptops.copy()
for idx, laptop1 in enumerate(og_laptops):
    if laptop1 in updated_laptops:
        speed1, ram1, hdd1 = laptop1[:-1] # Do not add price
        for laptop2 in og_laptops[idx:]:
            speed2, ram2, hdd2 = laptop2[:-1]
            if speed1 > speed2 and ram1 > ram2 and hdd1 > hdd2:
                if laptop2 in updated_laptops:
                    updated_laptops.remove(laptop2) # lp2 strictly < than lp1

            elif speed2 > speed1 and ram2 > ram1 and hdd2 > hdd1:
                updated_laptops.remove(laptop1) # lp1 strictly < than lp2
                break
            
cheapest_laptop = min(laptop[::-1] for laptop in updated_laptops)[::-1]
laptop_number = og_laptops.index(cheapest_laptop) + 1 # indexing from 1
print(laptop_number)
