##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Dice Tower                                               #
# Link: https://codeforces.com/contest/1266/submission/205667990 #
# Contest: 1266                                                  #
# Problem: B                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 30 ms                                                    #
# Memory: 136 KB                                                 #
##################################################################

# Explanation
#
# Every dice stacked on a tower is equal to 14, it's because
# the side that's facing down and the side that's facing up is always
# equal to 7, then:  21 - 7 = 14
#
# Except the last dice in the tower, because one side is facing up and that is
# not touching another dice, and that side can go between 1 and 6.
#
# If the last side can't be the numbers between 1 and 6, it does mean that
# tower doesn't exist

t = input()
integers = map(int, input().split())

for n in integers:
    if n < 14:
        result = 'NO'
 
    else:
        dices = n // 14
        total_in_dices =  dices * 14 + 7
        result = 'YES' if total_in_dices - n in [1, 2, 3, 4, 5, 6] else 'NO'
    
    print(result)
