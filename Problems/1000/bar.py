##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Bar                                                      #
# Link: https://codeforces.com/contest/56/submission/205932134   #
# Contest: 56                                                    #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# The minimum amount of people to check additionaly is just the sum
# of people drinking alcohol and people that are under age.

# Implementation
def n_check_addtl(info: list):
    alcohol_drinks = [
            'ABSINTH', 'BEER', 'BRANDY',
            'CHAMPAGNE', 'GIN', 'RUM',
            'SAKE', 'TEQUILA', 'VODKA',
            'WHISKEY', 'WINE']

    alcoholic_people = sum(info.count(drink) for drink in alcohol_drinks)
    under_age_people = sum(info.count(str(age)) for age in range(18))

    return alcoholic_people + under_age_people

# Program running
info = [input() for item in range(int(input()))]
min_to_check = n_check_addtl(info)
print(min_to_check)
