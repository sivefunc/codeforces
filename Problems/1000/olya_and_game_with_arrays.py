##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Olya and Game with Arrays                                #
# Link: https://codeforces.com/contest/1859/submission/218581870 #
# Contest: 1859                                                  #
# Problem: B                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 623 ms                                                   #
# Memory: 10032 KB                                               #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    a = [(input(), sorted(map(int, input().split())))[1] for i in range(n)]
    
    if n >= 2:
        lowest_i = sorted(a, key=lambda x: x[1])[0]
        lowest_idx = a.index(lowest_i)

        k = 0
        for idx, i in enumerate(a):
            if idx != lowest_idx:
                lowest_i.append(i[0])
                k += i[1]
        
        print(k + min(lowest_i))

    else:
        print(min(a[0]))
