##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: cAPS lOCK                                                #
# Link: https://codeforces.com/contest/131/submission/214253089  #
# Contest: 131                                                   #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

s = input()

if len(s) >= 2:
    result = s.lower() if s.isupper() else s[0].upper() + s[1:].lower() \
                if s[0].islower() and s[1:].isupper() else s
else:
    result = s.lower() if s.isupper() else s.upper()

print(result)
