##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Road to Zero                                             #
# Link: https://codeforces.com/contest/1342/submission/221329149 #
# Contest: 1342                                                  #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 32 KB                                                  #
##################################################################

for t_case in range(int(input())):
    x, y = map(int, input().split())
    a, b = map(int, input().split())
    
    dollars = min(x, y) * min(b, 2*a) + max(x - min(x, y), y - min(x, y)) * a
    print(dollars)
