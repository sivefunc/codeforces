##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Board Moves                                              #
# Link: https://codeforces.com/contest/1353/submission/221393252 #
# Contest: 1353                                                  #
# Problem: C                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 62 ms                                                    #
# Memory: 1564 KB                                                #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    result = sum(8*i**2 for i in range(1, n//2+1))
    print(result)
