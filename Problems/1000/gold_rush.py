##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Gold Rush                                                #
# Link: https://codeforces.com/contest/1829/submission/205377612 #
# Contest: 1829                                                  #
# Problem: D                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 374 ms                                                   #
# Memory: 10296 KB                                               #
##################################################################

# Run it on pypy, cpython too slow :C

# Explanation
#
# n = 27 and m = 4
#
# It finds the split of a pile such that a + 2a = pile
# where 'a' its the new pile and '2a' is the double of new pile
#
# This graph keeps going until '2a' is equal to m.
#
#
#                27
#               /  \
#              9   18
#             / \  / \
#            3  | |  12
#           /\  6 6 /  \
#          1  2 | | 4  8
#             --- ---  
#             /\   /\
#            2  4 2  4

# Implementation
def find_m(pile: int, m: int) -> bool:
    if pile == m:
        return True

    else:
        if pile % 3 != 0 or pile < m: # dead point, stop a side of graph
            return False
        
        # split the pile
        return find_m(pile // 3, m) or find_m((pile // 3) * 2, m)

# Program running
for t in range(int(input())):
    n, m = map(int, input().split()) # Original pile and pile with m nuggets
    result = 'yes' if find_m(n, m) else 'no'
    print(result)
