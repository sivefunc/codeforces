##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Beautiful String                                         #
# Link: https://codeforces.com/contest/1265/submission/205643354 #
# Contest: 1265                                                  #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 888 ms                                                   #
# Memory: 8784 kb                                                #
##################################################################

# do not run it on pypy it's way slower than cpython (in this problem)

# Explanation
#
# Replace each '?' character with a character in ['a', 'b', 'c'] that is not
# In the left or right side, e.g: a?b -> acb, because c is not in the left
# and is not in the right.
#
# Once we replace '?' we need to check the new string, because the initial
# Input given could have initially consecutive characters.

# Implementation
def is_beautiful(string: str) -> bool:
    for idx, ch in enumerate(string[:-1]):
        if ch == string[idx + 1]:
            return False

    return True

def construct_beautiness(string: str, chs=['a', 'b', 'c']) -> str:
    
    # We need greater than 1 because we are looking left and right
    if len(string) == 1:
        return 'a'
    
    # this just compare the ch before and after '?' to find the the new ch.
    for idx, ch in enumerate(string):
        if ch == '?':
            for k in chs:
                if idx == 0: # try exception is slower I think.
                    if k not in string[1]:
                        string = k + string[1:]

                
                elif idx == len(string) - 1:
                    if k not in string[-2]:
                        string = string[:-1] + k

                else:
                    if k not in string[idx - 1] and k not in string[idx + 1]:
                        string = string[:idx] + k + string[idx+1:]
                        break
    
    return string

# Program running
for t_case in range(int(input())):
    string = input()
    possible_string = construct_beautiness(string)
    result = possible_string if is_beautiful(possible_string) else -1
    print(result)
