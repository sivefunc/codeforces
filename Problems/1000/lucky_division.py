##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Lucky Division                                           #
# Link: https://codeforces.com/contest/122/submission/213532746  #
# Contest: 122                                                   #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

def almost_lucky(n: int) -> bool:
    for i in range(4, n+1):
        s = set(str(i))
        if len(s) == 2:
            if n % i == 0 and '4' in s and '7' in s:
                return True

        elif len(s) == 1:
            if n % i == 0 and ('4' in s or '7' in s):
                return True

    return False

n = int(input())
result = 'YES' if almost_lucky(n) else 'NO'
print(result)
