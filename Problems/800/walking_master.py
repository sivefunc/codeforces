##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Walking Master                                           #
# Link: https://codeforces.com/contest/1806/submission/209979697 #
# Contest: 1806                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# According to the movements in the problem:
#
# 1) You can't move down
# 2) You can't move to the right,  without moving up.
#
# This forms a domain of values where moving is possible
# It would look like this for a point A. (Try to do it on geogebra)
#
#    (X-1) and (X+1, Y+1) Domain:
#  . . . . . . . . . . . . , . ./x
#  . . . . . . . . . . . . , . /xx 
#  . . . . . . . . . . . . , ./xxx 
#  . . . . . . . . . . . . . /xxxx 
#  . . . . . . . . . . . . ./xxxxx 
# -------------------------Axxxxxx 
# xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
#
# Here the dots '.' represent the accepted values, and the 'x' represent
# the non accepted values. (the dots, and x's goes all the way up to infinity)
#
# As you can see, you can move all you wan't to the left
# Also because of the (X+1, Y+1) you can move in the segment formed by
# the positive slope.
#
# So if the point B is not in any of these places it means that the movement
# can't be done.
#
# Now, we want the minimum ammount of moves, to get to that possible point B
#
# That movement will always be going up to the 'By' coordinte
# (by moving 1 up, 1 right) then going to the 'Bx' coordinate (by moving
# to the left).
#
# This can be represented as a formula (|value| represent absolute value):
# |By - Ay| = Vertical moves 
# |Bx - (Ax + Vertical moves)|
#
# Some notes:
#
# 1) If the point B is in the slope, you don't have to move to the left.
# 2) You are adding the vertical moves to the Ax, because you are also
# moving to the right when going up to By.

# Implementation
def min_mvs(Xi: int, Yi: int, Xf: int, Yf: int) -> int:
    vertical_mvs = abs(Yf - Yi)
    Xm = Xi + vertical_mvs
    horizontal_mvs = abs(Xf - Xm)
    
    return -1 if (Yi > Yf) or (Xf > Xm) else vertical_mvs + horizontal_mvs

# Program running
for t_case in range(int(input())):
    Xi, Yi, Xf, Yf = map(int, input().split())
    result = min_mvs(Xi, Yi, Xf, Yf)
    print(result)
