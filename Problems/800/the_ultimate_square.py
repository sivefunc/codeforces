##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: The Ultimate Square                                      #
# Link: https://codeforces.com/contest/1748/submission/207871666 #
# Contest: 1748                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 77 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Look at the pattern
#
# 1 block = 1
# 2 blocks = 1
# 
# 3 blocks = 11
#            22
#
# 4 blocks = 11 or 22
#            22    22
#
# 5 blocks = 221
#            221
#            333
#
# 6 blocks = 221    221
#            221 or 333
#            333    333
# 
# 7 blocks = 2222
#            3331 
#            3331
#            4444
#
# 8 blocks = 2222
#            3331
#            4444
#            4444
#
# As you can see every two blocks the side lenght is increased by one
# so to get the lenght you divide the blocks by two
#
# If the blocks are odd, you need to round the result

# Program running
for t_case in range(int(input())):
    blocks = int(input())
    side_lenght = int(blocks / 2 + 0.5)
    print(side_lenght)
