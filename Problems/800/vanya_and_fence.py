##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Vanya and Fence                                          #
# Link: https://codeforces.com/contest/677/submission/213296604  #
# Contest: 677                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

n, h = map(int, input().split())
a = map(int, input().split())
result = sum(1 if i <= h else 2 for i in a)
print(result)
