##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: A1. Gardener and the Capybaras (easy version)            #
# Link: https://codeforces.com/contest/1775/submission/219705688 #
# Contest: 1775                                                  #
# Problem: A1                                                    #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 62 ms                                                    #
# Memory: 32 KB                                                  #
##################################################################

for t_case in range(int(input())):
    s = input()
    result = (s[0], s[1], s[2:]) if s[1] == 'a' else (s[0], s[1:-1], s[-1])
    print(*result)
