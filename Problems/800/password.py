##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Password                                                 #
# Link: https://codeforces.com/contest/1743/submission/209106076 #
# Contest: 1743                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 1731 ms                                                  #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Create all the sequence from 0 to 9999 and check if that sequence could
# belong to a sequence created by the used integers.
#
# What means a sequence created by the used integers?
#
# It means that only two different numbers are in there, and these different
# numbers get repeated twice.
#
# These numbers should be in the range given by the user.

# Program running
for t_case in range(int(input())):
    input()
    no_ints = input()
    used_integers = [str(i) for i in range(0, 10) if str(i) not in no_ints]
    sequences = 0
    
    # purely bruteforce
    for i in range(10000):
        sequence = (4 - len(str(i))) * '0' + str(i)
        ns_in_sq = set(sequence)
        
        if len(ns_in_sq) == 2:
            j, k = ns_in_sq
            if ((j in used_integers and k in used_integers) and
                (sequence.count(j) == 2 and sequence.count(k) == 2)):
                sequences += 1

    print(sequences)
