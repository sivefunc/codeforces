##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Atilla's Favorite Problem                                #
# Link: https://codeforces.com/contest/1760/submission/209579054 #
# Contest: 1760                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# We need to find the highest character in the string, because according
# to the problem you need an alphabet to write such string, and because
# the highest character contains all the characters (itself and below it)
# we need to do that.

# Old solution: https://codeforces.com/contest/1760/submission/209578867
# num_string = [ord(ch) - 96 for ch in string]
# result = max(num_string)

# Program running
for t_case in range(int(input())):
    input()
    string = input()
    max_ch = max(string)
    result = ord(max_ch) - 96
    print(result)
