##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Cowardly Rooks                                           #
# Link: https://codeforces.com/contest/1749/submission/210484601 #
# Contest: 1749                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# To know if you can move exactly one rook, you need to have a number of rooks
# that are less than N in total, because according to the problem
# these rooks can't attack each other, so having a slot free allows u to move
# your rook to that row (without changing row)

# Program running
for t_case in range(int(input())):
    n, rooks = map(int, input().split())
    for i in range(rooks): input()
    result = 'YES' if rooks < n else 'NO'
    print(result)
