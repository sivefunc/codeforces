##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Wrong Subtraction                                        #
# Link: https://codeforces.com/contest/977/submission/211972076  #
# Contest: 977                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

n, k = map(int, input().split())
for i in range(k):
    n = n-1 if int(str(n)[-1]) > 0 else n//10

print(n)
