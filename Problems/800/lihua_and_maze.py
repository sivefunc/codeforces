##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Li Hua and Maze                                          #
# Link: https://codeforces.com/contest/1797/submission/216343285 #
# Contest: 1797                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

def min_obs(n, m, x, y):
    if x in {1, n} or y in {1, m}:
        return 2 if x in {1, n} and y in {1, m} else 3
    return 4

# :)
for t_case in range(int(input())):
    n, m = map(int, input().split())
    x1, y1, x2, y2 = map(int, input().split())

    k1 = min_obs(n, m, x1, y1)
    k2 = min_obs(n, m, x2, y2)
    
    result = k1 if k1 <= k2 else k2
    print(result)
