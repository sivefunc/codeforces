##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Football                                                 #
# Link: https://codeforces.com/contest/96/submission/212451140   #
# Contest: 96                                                    #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 124 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

s = input()
print('YES' if '1'*7 in s or '0'*7 in s else 'NO')
