##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Extremely Round                                          #
# Link: https://codeforces.com/contest/1766/submission/206763477 #
# Contest: 1766                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# 1     2     3     4     5     6     7     8     9    = +9
# 10,   20,   30,   40,   50,   60,   70,   80,   90   = +9
# 100,  200,  300,  400,  500,  600,  700,  800,  900  = +9
# 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000 = +9
#
# As you can see the sequence increse by 9 for each decimal place
#
# If your number contains 4 decimal places e.g 4000
# then it would be 36 round numbers
#
# But we need to subtract some numbers because it's not 9000 and between
# 4000 and 9000 there are 5 numbers that we can't get.
#
# The numbers we are going to subtract is just the distance between 9 and
# the biggest decimal place (in this case is 4)

# Program running
for t_case in range(int(input())):
    n = input()
    round_ints = len(n) * 9 - (9 - int(n[0]))
    print(round_ints)
