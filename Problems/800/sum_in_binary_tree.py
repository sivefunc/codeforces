##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Sum in Binary Tree                                       #
# Link: https://codeforces.com/contest/1843/submission/212858050 #
# Contest: 1843                                                  #
# Problem: C                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 186 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

def sum_of_v(n: int) -> int:
    return (n + sum_of_v(n//2) if n != 1 else 1)

for t_case in range(int(input())):
    n = int(input())
    result = sum_of_v(n)
    print(result)
