##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Spy Detected!                                            #
# Link: https://codeforces.com/contest/1512/submission/216580797 #
# Contest: 1512                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    n = input()
    a = list(map(int, input().split()))

    b, c = set(a)
    result = 1 + (a.index(b) if a.count(b) == 1 else a.index(c))
    print(result)
