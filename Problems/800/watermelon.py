##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Watermelon                                               #
# Link: https://codeforces.com/contest/4/submission/208832403    #
# Contest: 4                                                     #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 62 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

n = int(input())
print('YES' if n % 2 == 0 and n != 2 else 'NO')
