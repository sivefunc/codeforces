##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: YES or YES?                                              #
# Link: https://codeforces.com/contest/1703/submission/203986350 #
# Contest: 1703                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# nothing to explain, why is this a contest problem?
for t in range(int(input())):print('yes' if 'yes' == input().lower() else 'no')
