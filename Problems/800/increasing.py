##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Increasing                                               #
# Link: https://codeforces.com/contest/1742/submission/209329400 #
# Contest: 1742                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 164 KB                                                 #
##################################################################

# Explanation
# 
# Reordering the elements of an array such that it's strictly increasing
# we need that each element is different in the array.
#
# because a sequence that goes like this:
# 1, 1, 1, 1
# Is not increasing, because it contains repetitive elements.

# Program running
for t_case in range(int(input())):
    n = int(input())
    array = input().split()
    result = 'YES' if n == len(set(array)) else 'NO'

    print(result)
