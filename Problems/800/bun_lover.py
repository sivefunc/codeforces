##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Bun Lover                                                #
# Link: https://codeforces.com/contest/1822/submission/206147958 #
# Contest: 1822                                                  #
# Problem: C                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 654 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# This solution was a lucky guess, because I knew that the border is N^2
# So I substracted the border of the output in the first 3 cases and that
# gave me the chocolate that's inside the border.
# 10, 12, 14 respectively. so after seeing that I just made the guess that
# 10, 12, 14 belongs to an sequence that I wrote as:
# 10 + 2 * (n - 4), where n - 4 represent how many times we are going to add
# the number 2 to the sequence.

# Program running
for t_case in range(int(input())):
    n = int(input())
    print(n**2 + 10 + 2 * (n - 4))
