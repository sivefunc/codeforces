##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Desorting                                                #
# Link: https://codeforces.com/contest/1853/submission/215220803 #
# Contest: 1853                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    a = list(map(int, input().split()))
    numbers = sorted(set(a))
    ops = 0
    if sorted(a) == a and n > 1:
        if len(numbers) > 1:
            min_dif = 1E9 + 1
            for idx, i in enumerate(numbers[:-1]):
                if numbers[idx+1] - i < min_dif:
                    min_dif = numbers[idx+1] - i

                if a.count(i) > 1 or a.count(numbers[idx+1]) > 1:
                    min_dif = 0
                    break
            
            ops = round((min_dif) / 2 + 0.9)

        else:
            ops = 1

    print(ops)
