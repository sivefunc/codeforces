##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Factorise N+M                                            #
# Link: https://codeforces.com/contest/1740/submission/207775991 #
# Contest: 1740                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# The output should be the input, because if u add the prime number to
# itself it would be 2 times the prime and that is divisible by two
# so it won't be prime.
#
# e.g: 11 + 11 = 2 * 11. you can divide that by 2.

# Program running
for t_case in range(int(input())):
    n = input()
    print(n)
