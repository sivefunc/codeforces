##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: United we Stand                                          #
# Link: https://codeforces.com/contest/1859/submission/218521075 #
# Contest: 1859                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 171 ms                                                   #
# Memory: 7756 KB                                                #
##################################################################

for t_case in range(int(input())):
    n = input()
    a = list(map(int, input().split()))

    b, c = list(filter(lambda x: x%2, a)), list(filter(lambda x: x%2==0, a))

    if b:
        if c:
            print(len(b), len(c))
            print(*b)
            print(*c)

        else:
            if len(set(b)) == 1:
                print(-1)

            else:
                for i in range(b.count(max(b))):
                    c.append(max(b))
                    b.remove(max(b))
                
                print(len(b), len(c))
                print(*b)
                print(*c)

    else:
        if len(set(c)) == 1:
            print(-1)

        else:
            for i in range(c.count(max(c))):
                b.append(max(c))
                c.remove(max(c))

            print(len(c), len(b))
            print(*c)
            print(*b)
