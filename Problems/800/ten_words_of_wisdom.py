##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Ten words of wisdom                                      #
# Link: https://codeforces.com/contest/1850/submission/215977235 #
# Contest: 1850                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    a = [list(map(int, input().split())) for i in range(int(input()))]
    b = sorted(filter(lambda x: x[0] <= 10, a), key=lambda x: x[1])

    r = a.index(b[-1]) + 1
    print(r)
