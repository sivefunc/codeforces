##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Beautiful Year                                           #
# Link: https://codeforces.com/contest/271/submission/214117459  #
# Contest: 271                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

n = int(input())
k = n+1

while True:
    if len(str(k)) == len(set(str(k))):
        break

    k += 1

print(k)
