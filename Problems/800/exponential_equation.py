##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Exponential Equation                                     #
# Link: https://codeforces.com/contest/1787/submission/217142674 #
# Contest: 1787                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    result = -1 if n % 2 else '1 ' + str(n//2)
    print(result)
