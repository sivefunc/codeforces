##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Twin Permutations                                        #
# Link: https://codeforces.com/contest/1831/submission/207627138 #
# Contest: 1831                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 280 ms                                                   #
# Memory: 252 KB                                                 #
##################################################################

# Explanation
#
# This was a hard one (30 min i think?)
#
# So we want to satisfy the condition a_i + b_i <= a_{i+1} + b_{i+1}
#
# So this is easy, you just need b_i+1 to be bigger enough that his sum.
# is greater than the sum before.
#
# But here we have a constraint in the problem
# The numbers of b need to be in the range [1, n] and can't be repeated
#
# So a lot of things came to my mind in that moment
# but i came to a conclusion:
#
# You have to add the lowest value of b to the highest value of a, and
# You have to add the highest value of b to the lowest value of a.
#
# repeat this with the second, third and so on.
#
# So this will make the sum of each term equal to the next term.
#
# a_i + b_i == a_{i+1} + b_{i+1} and this satisfy the condition.
#
# Now what would happen if you add a new constraint:
# each term is strictly less than the next: a_i + b_i < a_{i+1} + b_{i+1}
#
# This method wont work, and I think no other method would work.
# well obviously if the sequence is strictly ascending or descending you
# just have to add terms in a strictly ascending or descending way.

# Program running
for t_case in range(int(input())):
    n = int(input())
    a = [int(i) for i in input().split()]

    integers = sorted(a, reverse=True)
    b = [integers[i-1] for i in a]
    print(*b)
