##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Taisia and Dice                                          #
# Link: https://codeforces.com/contest/1790/submission/215375410 #
# Contest: 1790                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    n, s, r = map(int, input().split())
    sequence = [s-r] + [r // (n-1)] * (n-1)
    left = r % (n-1)
    for idx, i in enumerate(sequence):
        if i < (s - r):
            if left:
                sequence[idx] += 1
                left -= 1

            else:
                break

    print(*sequence)
