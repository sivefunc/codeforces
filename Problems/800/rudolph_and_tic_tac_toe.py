##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Rudolph and Tic-Tac-Toe                                  #
# Link: https://codeforces.com/contest/1846/submission/214522544 #
# Contest: 1846                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 155 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

def game_result(board: list) -> str:
    for horizontal in board:
        if len(set(horizontal)) == 1 and horizontal[0] != '.':
            return horizontal[0]

    for X in range(3):
        vertical = ''.join(board[Y][X] for Y in range(3))
        if len(set(vertical)) == 1 and vertical[0] != '.':
            return vertical[0]

    if board[0][0] == board[1][1] == board[2][2] and board[0][0] != '.':
        return board[0][0]

    if board[0][2] == board[1][1] == board[2][0] and board[0][2] != '.':
        return board[0][2]

    return 'DRAW'

for t_case in range(int(input())):
    board = input(), input(), input()
    print(game_result(board))
