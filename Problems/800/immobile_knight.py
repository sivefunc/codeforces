##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Immobile Knight                                          #
# Link: https://codeforces.com/contest/1739/submission/208499970 #
# Contest: 1739                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# This is an implementation problem, so we just need to go through each cell
# and find if the knight cant move, in the case we don't find that isolated
# cell, we just return a random cell.
#
# This method is slow but easier to understand.
# After I made my own solution, I went to the tutorial and found this faster
# solution: https://codeforces.com/blog/entry/107461
#
# (n // 2 + 1), (m // 2 + 1)

# Start of Functions declaration

# This is a part of a chess program that I made
# The program assume the board is NxN
# So we have to make a little modification
# To check for NxM
def knight_movements(X1, Y1, chess_board):
    """
    Return a list with all possible legal moves of the knight
    using his given position (X1, Y1) in a given chess_board
    """

    attacking_piece = chess_board[Y1][X1] 
    moves = [
                [X1 - 1, Y1 - 2], [X1 + 1, Y1 - 2],
                [X1 - 2, Y1 - 1], [X1 + 2, Y1 - 1],
                [X1 - 2, Y1 + 1], [X1 + 2, Y1 + 1],
                [X1 - 1, Y1 + 2], [X1 + 1, Y1 + 2],
            ] # Possible knight moves (Incluiding legal and ilegal)

    for move in moves.copy():
        X2, Y2 = move
        
        # Instead of len(chess_board), is len(chess_board[0])
        # Because we need the lenght of the row
        if (X2 < 0 or X2 >= len(chess_board[0]) or
            Y2 < 0 or Y2 >= len(chess_board)):
                # Move is not on the board
            moves.remove(move)

    return moves

def isolated_cell(board) -> tuple:
    for Y, row in enumerate(board):
        for X, column in enumerate(row):
            if not knight_movements(X, Y, board):
                return (Y+1, X+1)

    return (1, 1) # No isolated cell

# End of function declarations

# Program running
for t_case in range(int(input())):
    n, m = map(int, input().split())
    board = [[0] * m for row in range(n)]
    cell = isolated_cell(board)
    print(' '.join(str(i) for i in cell))
