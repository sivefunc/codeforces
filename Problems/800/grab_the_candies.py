##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Grab the Candies                                         #
# Link: https://codeforces.com/contest/1807/submission/208934801 #
# Contest: 1807                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 62 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# We can reorder the bags of candies in the way we want, so aligning
# the even bags first and then the odd bags will make Mihai always have
# more candies than bianca.
#
# Now the number of even candies if you sum that up it needs to be strictly
# greater than the odd candies, because it doesn't matter if u place
# the candies in that given order, bianca will have more in some
# point.

for t_case in range(int(input())):
    input()
    bags = list(map(int, input().split()))
    even_bags = [bag for bag in bags if bag % 2 == 0]
    odd_bags = [bag for bag in bags if bag % 2 != 0]

    result = 'YES' if sum(even_bags) > sum(odd_bags) else 'NO'
    print(result)
