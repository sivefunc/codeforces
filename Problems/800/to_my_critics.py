##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: To my Critics                                            #
# Link: https://codeforces.com/contest/1850/submission/214922970 #
# Contest: 1850                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    lower, medium, upper = sorted(map(int, input().split()))
    result = 'YES' if medium+upper >= 10 else 'NO'
    print(result)
