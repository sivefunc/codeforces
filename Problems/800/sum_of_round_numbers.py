##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Sum of round Numbers                                     #
# Link: https://codeforces.com/contest/1352/submission/217504601 #
# Contest: 1352                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 124 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    n = input()
    numbers = [d + '0'*(len(n[i:])-1) for i, d in enumerate(n) if d != '0']
    print(len(numbers))
    print(*numbers)
