##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Equal Candies                                            #
# Link: https://codeforces.com/contest/1676/submission/220987432 #
# Contest: 1676                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 155 ms                                                   #
# Memory: 9212 KB                                                #
##################################################################

for t_case in range(int(input())):
    n = input()
    a = list(map(int, input().split()))
    result = sum(i - min(a) for i in a)
    print(result)
