##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Way Too Long Words                                       #
# Link: https://codeforces.com/contest/71/submission/209705138   #
# Contest: 71                                                    #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Program running
for t_case in range(int(input())):
    word = input()
    result = word if len(word) <= 10 else word[0] + str(len(word)-2) + word[-1]
    print(result)
