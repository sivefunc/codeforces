##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Tenzing and Tsondu                                       #
# Link: https://codeforces.com/contest/1842/submission/214645460 #
# Contest: 1842                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 368 KB                                                 #
##################################################################

for t_case in range(int(input())):
    n, m = map(int, input().split())
    a = sum(map(int, input().split()))
    b = sum(map(int, input().split()))

    result = 'Tsondu' if a > b else 'Tenzing' if a != b else 'Draw'
    print(result)

