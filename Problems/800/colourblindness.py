##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Colourblindness                                          #
# Link: https://codeforces.com/contest/1722/submission/220252309 #
# Contest: 1722                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 62 ms                                                    #
# Memory: 20 KB                                                  #
##################################################################

for t_case in range(int(input())):
    input()
    s1 = input().replace('B', 'X').replace('G','X')
    s2 = input().replace('B', 'X').replace('G','X')
    result = 'YES' if s1==s2 else 'NO'
    print(result)
