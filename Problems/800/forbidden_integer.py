##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Forbidden Integer                                        #
# Link: https://codeforces.com/contest/1845/submission/216899599 #
# Contest: 1845                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 48 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

def solution(n: int, x: int) -> str:
    if x != 1:
        return str(n) + '\n' + ' '.join(['1'] * n)
    
    elif n == k and k != x:
        return '1' + '\n' + str(k)

    return str(n//2) + '\n' + ' '.join(['2']*(n//2-1) + [str(2 + n%2)])

for t_case in range(int(input())):
    n, k, x = map(int, input().split())
    
    print('NO' if n%2 and x==1 and k==2 or k == 1 else 'YES\n'+ solution(n, x))
