##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Destroyer                                                #
# Link: https://codeforces.com/contest/1836/submission/210123285 #
# Contest: 1836                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

def exists_arrangement(robots: list) -> bool:
    individual_reports = sorted(list(set(robots)))
    if len(individual_reports) == 1 and individual_reports[0] > 0:
        return False

    for idx, report in enumerate(individual_reports[:-1]):
        q1 = robots.count(report)
        q2 = robots.count(individual_reports[idx+1])

        if q2 > q1 or idx < report or report+1 < individual_reports[idx+1]:
            return False
    
    return True

for t_case in range(int(input())):
    input()
    robots = list(map(int, input().split()))
    result = 'YES' if exists_arrangement(robots) else 'NO'
    print(result)
