##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: SSeeeeiinngg DDoouubbllee                                #
# Link: https://codeforces.com/contest/1758/submission/207985881 #
# Contest: 1758                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# We add the string reversed to the end of the normal string, because
# because the first character must be equal to the last
# the second character must be equal to the penultimate
# the third character must be equal to the antepenultimate.
# and so on. so if u start placing the characters of the copy of normal s.
# you will end up with that.
#
# e.g
#
# ABC -> ABC+CBA -> ABCCBA

for t_case in range(int(input())):
    s = input()
    ss = s + s[::-1]
    print(ss)
