##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Prepend and Append                                       #
# Link: https://codeforces.com/contest/1791/submission/209498643 #
# Contest: 1791                                                  #
# Problem: C                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 204 KB                                                 #
##################################################################

# Explanation
#
# To recover the initial string, we need to make comparations
# in this order: the 1st and nth element, 2nd and (n-1)th, 3rd and (n-2th)
# and keep going.
#
# what are we going to compare?
# we are going to compare if each pair of digits are equal or not.
# 
# If they are equal print the integers between them (incluiding the
# compare pair)
#
# If they are not keep going.
#
# We do this process, because according to the problem every operation
# is doing on pairs, you add one element to the end left and one element to
# the end right, and these pair are different, so if you find a pair that is
# not eq, it mean that it's part of initial string.
#
# Note: After doing the comparations, if you don't find a pair that are equal
# it means that you can make that final string as ''

# Program running
for t_case in range(int(input())):
    n = int(input())
    final_str = input()
    
    og_str = ''
    for idx, left in enumerate(final_str[:int(n / 2 + 0.5)]):
        right = final_str[-idx-1]
        if left == right:
            og_str = final_str[idx:n-idx]
            break

    result = len(og_str)
    print(result)
