##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Serval and Inversion Magic                               #
# Link: https://codeforces.com/contest/1789/submission/215282680 #
# Contest: 1789                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 171 ms                                                   #
# Memory: 3056 KB                                                #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    s = input()
    l, r = 0, 0
    for idx in range(n//2):
        if s[idx] != s[~idx]:

            r = n - idx if not r else r
            l = n - idx - 1

        else:
            if l and r:
                break
    s = [i if j not in range(l, r) else '0' if i=='1' else '1'\
            for j, i in enumerate(s)]

    r = 'YES' if  s == s[::-1] else 'NO'
    print(r)
