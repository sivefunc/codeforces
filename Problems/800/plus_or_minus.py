##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Plus or Minus                                            #
# Link: https://codeforces.com/contest/1807/submission/208186794 #
# Contest: 1807                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# just need to compare .______.

# Program running
for t_case in range(int(input())):
    a, b, c = map(int, input().split())
    result = '+' if a + b == c else '-'
    print(result)
