##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Dalton the Teacher                                       #
# Link: https://codeforces.com/contest/1855/submission/216619432 #
# Contest: 1855                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 77 ms                                                    #
# Memory: 12084 KB                                               #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    a = list(map(int, input().split()))

    swap = [i for idx, i in enumerate(a) if idx+1 == i]
    print(int(len(swap) / 2 + 0.5))
