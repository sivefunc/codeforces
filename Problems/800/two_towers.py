##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Two Towers                                               #
# Link: https://codeforces.com/contest/1795/submission/208640889 #
# Contest: 1795                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Start moving blocks from one tower to another
# do it first on first tower to second tower
# then do it on second tower to first tower.
#
# For each move check if now both towers are beautiful.
#
# Moving a block to a tower and then moving that block to the older tower wont
# make a difference, because this is not a hanoi tower (only two towers exist)
# so you can't like "save" in a slot and then moving that to another slot.

# Start of Functions declaration
def is_bful(tower: str) -> bool:

    # This construct the beautiful tower without using a for loop
    # This is just for checking if new tower is beautiful
    RB = 'RB' * (len(tower)//2) + ('' if len(tower) % 2 == 0 else 'R')
    BR = 'BR' * (len(tower)//2) + ('' if len(tower) % 2 == 0 else 'B')
    
    return True if tower in {RB, BR} else False

def can_const(tower1: str, tower2: str) -> bool:
    for block in tower1[::-1][:-1]: # Looking for upper blocks:
        if block == tower2[-1]:
            return False # you can't place that block
        
        else:
            tower2 += block # Add upper block 
            tower1 = tower1[:-1] # Remove upper block
            if is_bful(tower2) and is_bful(tower1):
                # check if that move made the towers beautiful
                return True

    return False # u can't construct a pair of beautiness in these towers

# End of functions declaration

# Program running
for t_case in range(int(input())):
    input()
    tower1 = input()
    tower2 = input()

    result = 'YES' if ((is_bful(tower1) and is_bful(tower2)) or
            can_const(tower1, tower2) or can_const(tower2, tower1)) else 'NO'

    print(result)
