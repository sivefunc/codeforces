##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Soldier and Bananas                                      #
# Link: https://codeforces.com/contest/546/submission/211955306  #
# Contest: 546                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

k, n, w = map(int, input().split())
cost_w = k * sum(range(1, w+1))
result = 0 if cost_w <= n else cost_w - n
print(result)
