##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Divisible Array                                          #
# Link: https://codeforces.com/contest/1828/submission/207371794 #
# Contest: 1828                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 62 ms                                                    #
# Memory: 24 KB                                                  #
##################################################################

# Explanation
#
# We make a increasing sequence that goes from 1 to n, why? because
# every number in it is divisible by the index + 1
#
# The first condition is satisfied: ai is divisible by i for all 1<=i<=n
#
# Now, we check if the sum of that sequence is divisible by n, if its not
# we just keep adding 1 to the first number, until it gets divisible.
#
# Because a number divided by 1 is the number, so at the end we just start
# adding 1 to the sum of the array.

# Program running
for t_case in range(int(input())):
    n = int(input())
    numbers = [i for i in range(1, n+1)]
    while sum(numbers) % n:
        numbers[0] += 1

    print(' '.join(str(i) for i in numbers))
