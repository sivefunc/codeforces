##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Petya and Strings                                        #
# Link: https://codeforces.com/contest/112/submission/211169959  #
# Contest: 112                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

s1 = input().lower()
s2 = input().lower()

result = "-1" if s1 < s2 else '1' if s2 < s1 else '0'
print(result)
 
