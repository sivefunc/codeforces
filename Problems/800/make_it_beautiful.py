##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Make It Beautiful                                        #
# Link: https://codeforces.com/contest/1783/submission/219054345 #
# Contest: 1783                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 233 ms                                                   #
# Memory: 9496 KB                                                #
##################################################################

def is_beautiful(ints: list) -> bool:
    for idx, i in enumerate(ints):
        if sum(ints[:idx]) == i:
            return False

    return True

for t_case in range(int(input())):
    n = int(input())
    ints = sorted(map(int, input().split()), reverse=True)

    if len(set(ints)) == 1:
        print('NO')

    else:
        while ints[0] == ints[1]:
            ints += [ints.pop(0)]
        
        if is_beautiful(ints):
            print('YES\n', *ints)

        else:
            print('NO')
