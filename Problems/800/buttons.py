##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Butons                                                   #
# Link: https://codeforces.com/contest/1858/submission/219243515 #
# Contest: 1858                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 218 ms                                                   #
# Memory: 8744 KB                                                #
##################################################################

for t_case in range(int(input())):
    a, b, c = list(map(int, input().split()))

    result = 'First' if a + c%2 > b else 'Second'
    print(result)
