##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Triple                                                   #
# Link: https://codeforces.com/contest/1669/submission/220550412 #
# Contest: 1669                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 373 ms                                                   #
# Memory: 26816 KB                                               #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    a = sorted(list(map(int, input().split())))
    x = -1

    for idx, i in enumerate(a[:-2]):
        if i == a[idx+1] and i == a[idx+2]:
            x = i
            break

    print(x)
