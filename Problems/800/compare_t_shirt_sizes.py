##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Compare T-Shirt Sizes                                    #
# Link: https://codeforces.com/contest/1741/submission/208629429 #
# Contest: 1741                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Transform the letters into numerical values and compare, if the last letter
# is 'S' u have to transform result to negative

def value(size):
    X_VALUE = size.count('X') if size[-1] == 'L' else -size.count('X')
    return X_VALUE + (-1 if size[-1] == 'S' else 0 if size[-1] == 'M' else 1)

for t_case in range(int(input())):
    
    a, b = input().split()
    r = '>' if value(a) > value(b) else '<' if value(a) != value(b) else '='
    print(r)

