##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Beautiful Sequence                                       #
# Link: https://codeforces.com/contest/1810/submission/215055698 #
# Contest: 1810                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 78 ms                                                    #
# Memory: 164 KB                                                 #
##################################################################

def is_beautiful(a: list, n: int) -> str:
    b = a[::-1]
    for i in set(a):
        if n + ~b.index(i) >= i-1:
            return 'YES'
    return 'NO'

for t_case in range(int(input())):
    n = int(input())
    a = list(map(int, input().split()))
    print(is_beautiful(a, n))
