##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Morning Sandwich                                         #
# Link: https://codeforces.com/contest/1849/submission/218801220 #
# Contest: 1849                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 3348 KB                                                #
##################################################################

for t_case in range(int(input())):
    b, c, h = map(int, input().split())

    result = 2*c + 2*h + 1 if b > c+h else 2*b - 1
    print(result)
