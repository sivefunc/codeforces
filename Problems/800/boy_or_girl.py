##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Boy or Girl                                              #
# Link: https://codeforces.com/contest/236/submission/206970530  #
# Contest: 236                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
# just use set() to get unique characters

print('CHAT WITH HER!' if len(set(input())) % 2 == 0 else 'IGNORE HIM!')
