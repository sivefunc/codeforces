##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Love Story                                               #
# Link: https://codeforces.com/contest/1829/submission/206283492 #
# Contest: 1829                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# to look the characters that differ just compare side by side the strings
#
# |c|o|d|e|f|o|r|c|e|s|
# |-|-|-|-|-|-|-|-|-|-|
# |r|e|c|u|r|s|i|v|e|s|
#
# c and r, o and e, d and c, e and u, f and r, o and s, r and i, you keep going

og_string = 'codeforces'
for t_case in range(int(input())):
    dif_string = input()
    n_indices = len([ch for idx, ch in enumerate(dif_string) \
            if ch != og_string[idx]])
    
    print(n_indices)
