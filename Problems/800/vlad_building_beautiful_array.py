##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Vlad Building Beautiful Array                            #
# Link: https://codeforces.com/contest/1833/submission/218855653 #
# Contest: 1833                                                  #
# Problem: C                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 483 ms                                                   #
# Memory: 26904 KB                                               #
##################################################################

def can_be(a: list, even: list, odd: list) -> bool:
    result = True
    for a_i in set(a):
        if a_i % 2 == 0:
            if odd[0] > a_i:
                return False
    return True

for t_case in range(int(input())):
    input()
    a = list(map(int, input().split()))

    o = sorted(set(filter(lambda x: x%2, a)))
    e = sorted(set(filter(lambda x: x%2 == 0, a)))
    
    print('YES' if (o and not e or e and not o) or can_be(a, e, o) else 'NO')
