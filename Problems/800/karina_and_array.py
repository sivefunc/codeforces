##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Karina and Array                                         #
# Link: https://codeforces.com/contest/1822/submission/206138744 #
# Contest: 1822                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 186 ms                                                   #
# Memory: 26108 KB                                               #
##################################################################

# Explanation
#
# just find the two longest number of array
# and multiply them, because we are deleting the numbers between them
# to get the max beauty pair
#
# we need also need to find the shortest number of array (negative number)
# because negative number * negative number = positive number
#
# just compare the products, you could also compare the digits and predict
# the product but code would be larger

# Implementation
for t_case in range(int(input())):
    n = input() # lenght of array
    a = sorted(map(int, input().split()))

    n1, n2 = a[0], a[1] # max negative numbers
    p1, p2 = a[-1], a[-2] # max positive numbers

    # I learned later that u could also use max(p1*p2, n1*n2)
    result = p1*p2 if n1*n2 == p1*p2 else n1*n2 if n1*n2 > p1*p2 else p1*p2
    print(result)
