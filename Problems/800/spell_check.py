##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Spell Check                                              #
# Link: https://codeforces.com/contest/1722/submission/220044462 #
# Contest: 1722                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 140 ms                                                   #
# Memory: 5816 KB                                                #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    s = input()
    result = 'YES' if sorted(s) == sorted('Timur') else 'NO'
    print(result)
