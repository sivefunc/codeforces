##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Division?                                                #
# Link: https://codeforces.com/contest/1669/submission/220405784 #
# Contest: 1669                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 233 ms                                                   #
# Memory: 8464 KB                                                #
##################################################################

for t_case in range(int(input())):
    rating = int(input())
    if rating <= 1399:
        print('Division 4')

    elif 1400 <= rating <= 1599:
        print('Division 3')

    elif 1600 <= rating <= 1899:
        print('Division 2')

    else:
        print('Division 1')
