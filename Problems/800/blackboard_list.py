##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Blackboard List                                          #
# Link: https://codeforces.com/contest/1838/submission/208484567 #
# Contest: 1838                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 140 KB                                                 #
##################################################################

for t_case in range(int(input())):
    input()
    integers = {int(i) for i in input().split()}
    initial_list = sorted(list(integers), reverse=True)
    if initial_list[-1] >= 0:
        v = initial_list[0]
    else:
        v = initial_list[-1]
    print(v)

 
