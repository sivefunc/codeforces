##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Sum                                                      #
# Link: https://codeforces.com/contest/1742/submission/207660771 #
# Contest: 1742                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 108 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# just check each condition, we can do this because input is limited
# to 3 values.

# Program running
for t_case in range(int(input())):
    a, b, c = map(int, input().split())
    result = 'YES' if a == b+c or b == a+c or c == a+b else 'NO'
    print(result) # why is this a codeforces problem?
