##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Polycarp and the Day of Pi                               #
# Link: https://codeforces.com/contest/1790/submission/206991270 #
# Contest: 1790                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Just compare the digits from start and stop when digits are different.

pi = '314159265358979323846264338327'
for t_case in range(int(input())):
    string = input()
    digits = 0
    for idx, digit in enumerate(string):
        if digit != pi[idx]:
            break
        digits += 1
    print(digits)
