##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Typical Interview Problem                                #
# Link: https://codeforces.com/contest/1796/submission/219385579 #
# Contest: 1796                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 171 ms                                                   #
# Memory: 10012 Kb                                               #
##################################################################

fb = 'FBFFBFFBFBFFBFFBFBFFBFFBFBFFBFFBFBFFBFF'
for t_case in range(int(input())):
    input()
    s = input()
    result = 'YES' if s in fb else 'NO'
    print(result)
