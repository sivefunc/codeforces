##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Long Long                                                #
# Link: https://codeforces.com/contest/1843/submission/214420209 #
# Contest: 1843                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 171 ms                                                   #
# Memory: 28008 KB                                               #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    a = list(map(int, input().split()))
    b = ''.join('P' if i > 0 else '' if i == 0 else 'N' for i in a).split('P')
   
    max_sum = sum(abs(i) for i in a)
    min_ops = len(b) - b.count('')

    print(max_sum, min_ops)
