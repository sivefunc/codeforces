##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Sequence Game                                            #
# Link https://codeforces.com/contest/1862/submission/220208419  #
# Contest: 1862                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 389 ms                                                   #
# Memory: 33904 KB                                               #
##################################################################

for t_case in range(int(input())):
    n = input()
    b = list(map(int, input().split()))
    c = []
    for idx, i in enumerate(b[:-1]):
        c.append(i)
        if i > b[idx+1]:
            c.append(b[idx+1])
    
    c.append(b[-1])
    print(len(c))
    print(*c)
