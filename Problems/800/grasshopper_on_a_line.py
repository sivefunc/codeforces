##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Grasshopper on a Line                                    #
# Link: https://codeforces.com/contest/1837/submission/214971774 #
# Contest: 1837                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    x, k = map(int, input().split())

    result = f"2\n{x+1} -1" if x % k == 0 else f"1\n{x}"
    print(result)
