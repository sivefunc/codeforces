##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Insert Digit                                             #
# Link: https://codeforces.com/contest/1811/submission/209754988 #
# Contest: 1811                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 612 KB                                                 #
##################################################################

# Explanation
#
# To construct the biggest number, we need to start placing the digit going
# from left to right and check if placing the digit there at index 'i' is
# bigger than placing at 'i+1'.
#
# We do this because according to the positional numeric system, adding
# a number to the left is bigger than adding it to the right.   

# Implementation
def max_s(s: str, d: str, n: int) -> str:
    for idx, k in enumerate(s):
        if d > k:
            return s[:idx] + d + s[idx:]
    return s + d

# Program running
for t_case in range(int(input())):
    n, d = input().split()
    s = input()
    result = max_s(s, d, int(n))

    print(result)
