##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: LuoTianyi and the Palindrome String                      #
# Link: https://codeforces.com/contest/1825/submission/206286947 #
# Contest: 1825                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# A palindrome has the property that each character in a position i, is eq.
# to ch in position i (but being backwards the word), it would be 
# first and last, second and penultimate, third and antepenultimate, and so on.
#
# So if we remove the first or last character the palindrome will break and we
# will get the longest subsequence that is not palindrome, because palindorme
# condition won't hold anymore, because every character will be shifted to one
# place left or right, depends if we are removing the last or first character.
#
# e.g: CCACC -> CCAC, the first and last are equal but because everything was
# shifted, second and third are not equal.
#
# In the case that all letters are equal, return -1.

# Program running
for t_case in range(int(input())):
    s = input()
    result = len(s[:-1]) if s[:-1] != s[:-1][::-1] else -1
    print(result)
