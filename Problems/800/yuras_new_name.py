##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Yura's New Name                                          #
# Link: https://codeforces.com/contest/1820/submission/214982656 #
# Contest: 1820                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

def yura(s: str, ops: int) -> int:
    if len(s) >= 3:
        if s == '^_^':
            return ops

        if s[0] == '^':
            if s[1] == '^':
                return yura(s[1:], ops)
            return yura('^'+s[2:], ops+1) if s[2] == '_' else yura(s[2:], ops)
        
        return yura('^'+s, ops+1)
    
    elif len(s) == 2:
        return ops + (0 if s == '^^' else 1 if s in '_^^_' else 3)

    return ops + (1 if s == '^' else 2)

for t_case in range(int(input())):
    s = input()
    print(yura(s, 0))
