##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Hayato and School                                        #
# Link: https://codeforces.com/contest/1780/submission/207287655 #
# Contest: 1780                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 186 ms                                                   #
# Memory: 916 KB                                                 #
##################################################################

# Explanation
#
# There are only two ways to know if the sum of 3 numbers will be an odd number
#
# 1) ODD + ODD + ODD == ODD == 1 + 2 + (EVEN + EVEN + EVEN)
# 2) ODD + EVEN + EVEN == 1 + (EVEN + EVEN + EVEN)
#
# So we just need to find (3 odd numbers) or (2 even numbers and 1 odd number)

# Program running
for t_case in range(int(input())):
    n = input()
    a = [int(i) for i in input().split()]
    
    odds = [str(idx+1) for idx, n in enumerate(a) if n % 2 != 0]
    evens = [str(idx+1) for idx, n in enumerate(a) if n % 2 == 0]
    
    if odds:
        if len(evens) >= 2:
            result = 'YES\n' + ' '.join((odds[0],*evens[:2]))

        else:
            if len(odds) >= 3:
                result = 'YES\n' + ' '.join(odds[0:3])

            else:
                result = 'NO'
    else:
        result = 'NO'

    print(result)

# did u know that 1 + 1 == 2? 
