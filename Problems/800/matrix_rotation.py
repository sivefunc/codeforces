##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Matrix Rotation                                          #
# Link: https://codeforces.com/contest/1772/submission/206650300 #
# Contest: 1772                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Just check the conditions in statement, if not true then just keep rotating
# until condition is true  (max 3 rotations), after that these 3 rotations
# the matrix is not beautiful.

# Implementation
def is_beautiful(m: list) -> bool:
    if (m[0][0] < m[0][1] and
        m[1][0] < m[1][1] and
        m[0][0] < m[1][0] and
        m[0][1] < m[1][1]):
        
        return True
    return False

# I made this for a rubik solver, now I reused it.
def rotate_face(face: list, clockwise=1) -> list:
    ROWS = len(face)
    COLUMNS = len(face[0])
    
    if clockwise:
        # Transform the columns into rows starting from 0th column and -1th row
        face = [[face[back_row][column] for back_row in \
                range(-1, -(ROWS + 1), -1)] for column in range(COLUMNS)]
   
    else: # anti clockwise
        # Transform the columns into rows starting from -1th column and 0th row
        face = [[face[row][back_column] for row in range(ROWS)]\
                for back_column in range(-1, -(COLUMNS + 1), -1)]
   
    return face

# Program running
for t_case in range(int(input())):
    m = [
        [int(n) for n in input().split()],
        [int(n) for n in input().split()]
            ]
    
    if is_beautiful(m):
        print('YES')

    else:
        result = 'NO'
        for rotation in range(3): # also could use any(list comprehension)
            if is_beautiful(m := rotate_face(m)):
                result = 'YES'
                break

        print(result)
