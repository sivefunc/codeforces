##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Everybody Likes Good Arrays!                             #
# Link: https://codeforces.com/contest/1777/submission/217941930 #
# Contest: 1777                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 124 ms                                                   #
# Memory: 156 KB                                                 #
##################################################################

def is_good(a: list) -> bool:
    for idx, i in enumerate(a[:-1]):
        if i % 2 == a[idx+1] % 2:
            return False
    
    return True

for t_case in range(int(input())):
    n = input()
    a = list(map(int, input().split()))

    cnt = 0
    while not is_good(a):
        for idx, i in enumerate(a[:-1]):
            if i % 2 == a[idx+1] % 2:
                a.insert(idx, a.pop(idx) * a.pop(idx))
                cnt += 1
                break

    print(cnt)
