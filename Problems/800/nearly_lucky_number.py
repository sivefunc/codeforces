##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Nearly Lucky Number                                      #
# Link: https://codeforces.com/contest/110/submission/216469883  #
# Contest: 110                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

n = input()
lucky_digits = n.count('4') + n.count('7')
print('YES' if lucky_digits in {4, 7} else 'NO')
