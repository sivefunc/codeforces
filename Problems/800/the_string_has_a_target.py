##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: The String has a Target                                  #
# Link: https://codeforces.com/contest/1805/submission/218992211 #
# Contest: 1805                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 327 ms                                                   #
# Memory: 9528 KB                                                #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    s = input()
    letters = sorted(set(s))

    for idx, i in enumerate(s[::-1]):
        if i == letters[0]:
            s = i + s[:~idx] + s[n-idx:]
            break

    print(s)
