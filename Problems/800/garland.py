##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Garland                                                  #
# Link: https://codeforces.com/contest/1809/submission/210905475 #
# Contest: 1809                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# There are only 5 possible light bulbs configurations:
#
# 1111 = 0 operations
# 1234 = 4 operations
# 1123 = 4 operations
# 1122 = 4 operations
# 1112 = 6 operations
#
# so we just need to identify the corresponding sequence.

# Program running
for t_case in range(int(input())):
    s = input()
    result = -1 if len(set(s)) == 1 else 6 if len(set(s)) == 2 and \
            s.count(s[0]) in {1, 3} else 4
    print(result)
