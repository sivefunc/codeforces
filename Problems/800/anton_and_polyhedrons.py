##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Anton and Polyhedrons                                    #
# Link: https://codeforces.com/contest/785/submission/217705869  #
# Contest: 785                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 218 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

faces = {
    'tetrahedron':4,
    'cube':6,
    'octahedron':8,
    'dodecahedron':12,
    'icosahedron':20
        }

print(sum(faces[input().lower()] for polyhedron in range(int(input()))))
