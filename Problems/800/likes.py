##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Likes                                                    #
# Link: https://codeforces.com/contest/1802/submission/216117445 #
# Contest: 1802                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 61 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    b = list(map(int, input().split()))

    n_v = len(list(filter(lambda x: x<0, b)))
    p_v = n - n_v

    a1 = list(range(1, p_v + 1)) + list(range(p_v - 1, p_v - n_v - 1, -1)) 
    a2 = [1, 0] * n_v + list(range(1, n - (n_v * 2) + 1))

    print(*a1,'\n',*a2,)
