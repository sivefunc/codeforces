##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Sasha and Array Coloring                                 #
# Link: https://codeforces.com/contest/1843/submission/212012818 #
# Contest: 1843                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    a = sorted(map(int, input().split()))
    
    result = sum(a[~idx] - a[idx] for idx, i in enumerate(range(n//2)))
    print(result)
