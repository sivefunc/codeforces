##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Yes-Yes?                                                 #
# Link: https://codeforces.com/contest/1759/submission/207481024 #
# Contest: 1759                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Instead of going character by character and trying to determine if that could
# belong to 'Yes', i found an easier way to do it (could be slower? idk)
#
# Just create a long string (it would be original) consisting of 'Yes' and
# check if input belongs to that long string.

# Program running
for t_case in range(int(input())):
    s = input()
    lazy_evaluation = 'Yes' * len(s)
    result = 'YES' if s in lazy_evaluation else 'NO'

    print(result)
