##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Add Plus Minus Sign                                      #
# Link: https://codeforces.com/contest/1774/submission/217346884 #
# Contest: 1774                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    a = input()
    
    s = ''
    sign = '+'
    for idx, i in enumerate(a[:-1]):
        if i == '1':
            sign = '-' if sign == '+' else '+'
        
        s += sign

    print(s)
