##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Is It a Cat?                                             #
# Link: https://codeforces.com/contest/1800/submission/206389236 #
# Contest: 1800                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 124 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# According to the conditions, we just replace each sequence of characters
# with the character, e.g -> mmmmmmm -> m
#
# After replacing the string should be meow, because every ch in
# meow can be expanded, e.g -> meow: mmmm, eeee, oo, www
#
# If a ch in meow is not in og string it means output -> no
# If a ch in string is not in meow it means output -> no
# If a ch is not continous in the string output -> no
# mmemm, m is not continous.

for t_case in range(int(input())):
    n = input()
    s = input().lower()

    for ch in 'meow':
        s = s.replace(s.count(ch) * ch, ch) if ch in s else ''

    result = 'YES' if s == 'meow' else 'NO'
    print(result)
