##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Anton and Danik                                          #
# Link: https://codeforces.com/contest/734/submission/212987303  #
# Contest: 734                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 124 KB                                                 #
##################################################################

n = int(input())
s = input()
A = s.count('A')
D = s.count('D')

result = 'Anton' if A > D else 'Danik' if D > A else 'Friendship' 
print(result)
