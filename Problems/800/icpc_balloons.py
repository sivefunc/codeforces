##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: ICPC Balloons                                            #
# Link: https://codeforces.com/contest/1703/submission/220862649 #
# Contest: 1703                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 61 ms                                                    #
# Memory: 1576 KB                                                #
##################################################################

for t_case in range(int(input())):
    input()
    s = input()

    given = []
    balloons = 0
    for i in s:
        if i in given:
            balloons += 1

        else:
            balloons += 2
            given.append(i)

    print(balloons)
