##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Politics                                                 #
# Link: https://codeforces.com/contest/1818/submission/215123365 #
# Contest: 1818                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 77 ms                                                    #
# Memory: 128 KB                                                 #
##################################################################

# Another solution after reading the editorial
# https://codeforces.com/contest/1818/submission/215123886

###########################################################

# Working solution before reading the editorial
# https://codeforces.com/contest/1818/submission/215123365
def MBR_INTRF(members: list, p_r: list, n_r: list) -> list:
    interfering = []
    for idx1, member in enumerate(members):
        interference = [0, idx1]
        for idx2, (m_d, p_d) in enumerate(zip(member, members[0])):
            if p_d != m_d:
                if p_r[idx2] == n_r[idx2]:
                    interference[0] += 1

                elif p_r[idx2] > n_r[idx2]:
                    if m_d == '+' and p_d == '-':
                        interference[0] += 1
        
                else:
                    if m_d == '-' and p_d == '+':
                        interference[0] += 1
        
        if interference != [0, idx1]:
            interfering.append(interference[:])

    return sorted(interfering, reverse=True)

def del_mbrs(members: list, interfering: list, idx: int, count: int) -> None:
    if count == 0:
        return None

    for member in interfering.copy():
        if member[1] in members and members[member[1]][idx] != members[0][idx]:
            interfering.remove(member)
            members.remove(members[member[1]])
            count -= 1
            if count == 0:
                break


for t_case in range(int(input())):
    n, k = map(int, input().split())
    m = [input() for i in range(n)]
    p_r = [sum(m[k][i] == '+' for k in range(n)) for i in range(k)]
    n_r = [sum(m[k][i] == '-' for k in range(n)) for i in range(k)]
    interfering = MBR_INTRF(m, p_r, n_r)
    
    for idx, opinion in enumerate(m[0]):
        if p_r[idx] > n_r[idx]:
            if opinion == '-':
                del_mbrs(m, interfering, idx, abs(p_r[idx] - n_r[idx]) + 1)

        elif p_r[idx] < n_r[idx]:
            if opinion == '+':
                del_mbrs(m, interfering, idx, abs(p_r[idx] - n_r[idx]) + 1)
        
        else:
            del_mbrs(m, interfering, idx, abs(p_r[idx] - n_r[idx]) + 1)

    for idx, opinion in enumerate(m[0]):
        for member in m.copy():
            if opinion != member[idx]:
                m.remove(member)
        
    print(len(m))
