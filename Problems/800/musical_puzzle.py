##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Musical Puzzle                                           #
# Link: https://codeforces.com/contest/1833/submission/206469225 #
# Contest: 1833                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 264 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# To find the notes, we just take the two characters in the beginning, save it
# as a note, then take the last character of note and find the next character
# in the array and add it to that last character, we do this because a merge
# of two notes, it's just the first ch of 1st note and last ch of 
# 2nd note, between these characters you place last ch of 1st note or first ch
# of 2nd note (That middle character is like the bridge between two notes, so
# to merge you need that common bridge). and keep doing that process.
#
# e.g: abcdefg
#
# As u can notice a lot of bridges there.
# ab - bc - cd - de - ef - fg
#
# It's kind of hard to explain it, but it's easy to see it.

# Implementation
for t_case in range(int(input())):
    input()
    s = input()
    melodies = set()
    melody = ''
    for ch in s:
        melody += ch
        if len(melody) == 2:
            melodies.add(melody)
            melody = melody[-1]

    print(len(melodies))
