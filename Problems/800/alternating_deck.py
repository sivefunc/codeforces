##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: A2. Alternating Deck (Hard version)                      #
# Link: https://codeforces.com/contest/1786/submission/218368690 #
# Contest: 1786                                                  #
# Problem: A2                                                    #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 77 ms                                                    #
# Memory: 2716 KB                                                #
##################################################################

def give_cards(player, last_color, last_number, n):
    cards = last_number*2 + 7
    value = n if cards >= n else cards
    player[0] += value//2
    player[1] += value//2
    player[0 if last_color == 'black' else 1]  += value%2
    last_color = 'white' if last_color == 'black' else 'black'
    n -= n if cards >= n else cards

    return last_color, n

for t_case in range(int(input())):
    n = int(input())

    if n <= 6:
        alice = [1, 0]
        bob = [(n-1)//2, (n-1)//2 + (n-1) % 2]

    else:
        n -= 6
        alice = [1, 0] # White and black respectively
        bob = [2, 3]

        a_last = 1
        b_last = 3
        last_color = 'black'

        while n:
            last_color, n = give_cards(alice, last_color, a_last, n)
            if n: last_color, n = give_cards(bob, last_color, b_last, n)
            a_last += 4
            b_last += 4

    print(*alice, *bob)
