##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: A+B?                                                     #
# Link: https://codeforces.com/contest/1772/submission/207408278 #
# Contest: 800                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
# 
# Just separate the expresion
# why is this a codeforces contest problem???????

for t_case in range(int(input())):
    result = sum(map(int, input().split('+')))
    print(result)
