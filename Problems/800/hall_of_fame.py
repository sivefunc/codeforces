##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Hall of Fame                                             #
# Link: https://codeforces.com/contest/1779/submission/206882986 #
# Contest: 1779                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 336 KB                                                 #
##################################################################

# Explanation
#
# A sequence of L and R 3 things could happen:
#
# RL appears and that lights everything.
# LR appears and swaping it, it will light everything
# None of that appear, it won't light. 

for t_case in range(int(input())):
    n = input()
    trophies = input()

    if 'L' in trophies and 'R' in trophies:
        # I forgot that we start from 1 and not from 0 in codeforces
        result = 0 if 'RL' in trophies else trophies.index('LR') + 1

    else:
        result = -1

    print(result)
