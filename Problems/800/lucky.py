##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Lucky?                                                   #
# Link: https://codeforces.com/contest/1676/submission/219918573 #
# Contest: 1676                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 109 ms                                                   #
# Memory: 4212 KB                                                #
##################################################################

for t_case in range(int(input())):
    s = input()
    s1 = sum(map(int, list(s[:3])))
    s2 = sum(map(int, list(s[3:])))
    result = 'YES' if s1 == s2 else 'NO'
    print(result)
