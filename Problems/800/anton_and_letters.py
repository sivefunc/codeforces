##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Anton and Letters                                        #
# Link: https://codeforces.com/contest/443/submission/213695561  #
# Contest: 443                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 62 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

k = set(input()[1:-1].split(', '))
print(len(k) if k != {''} else 0)
