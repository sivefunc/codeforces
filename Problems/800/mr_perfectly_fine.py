##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Mr. Perfectly Fine                                       #
# Link: https://codeforces.com/contest/1829/submission/206281150 #
# Contest: 1829                                                  #
# Problem: C                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 265 ms                                                   #
# Memory: 9404 KB                                                #
##################################################################

# Explanation
#
# 11 = 2 skills
# 10 = 1 skill
# 01 = 1 skill
#
# if a pile of books with 11 skills exists find the min. time to read it
# and compare it to the sum of min. times in the books with 10 and 01 skills
#
# else find the books with 10 and 01 skills, and pick his min sum.

# Implementation
def min_time_to_become(books: dict) -> int:
    if books['11']: # pile of 11 skills exists
        for book in '10', '01':
            if not books[book]: # pile of skill 10 or skill 01 doesn't exist
                return min(books['11'])

        # All 3 piles exist, find the minimum time in each skill pile.
        s11, s10, s01 = min(books['11']), min(books['10']), min(books['01'])
        return s11 if s11 <= s10 + s01 else s10 + s01 # u could also use min()

    else: # pile of 11 skills doesn't exist
        for book in '10', '01':
            if not books[book]:
                return -1 # You can't have two skills.

        return min(books['10']) + min(books['01'])

# Running the program
for t_case in range(int(input())):
    books = {'00': [], '10': [], '01': [], '11': []} # The order of skill books
    for book in range(int(input())):
        time, skill = input().split()
        books[skill] += [int(time)] # add time to each skill pile
    
    result = min_time_to_become(books)
    print(result)
