##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Magnets                                                  #
# Link: https://codeforces.com/contest/344/submission/214413280  #
# Contest: 344                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 218 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

before_magnet = 0
groups = 0

for i in range(int(input())):
    magnet = input()
    if before_magnet != magnet:
        groups += 1
        before_magnet = magnet

print(groups)
