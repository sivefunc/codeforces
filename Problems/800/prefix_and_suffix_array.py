##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Prefix and Suffix Array                                  #
# Link: https://codeforces.com/contest/1794/submission/210181519 #
# Contest: 1794                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# We need to find the longest suffix and prefix and join them to form 's'
# and check if 's' is palindrome.

for t_case in range(int(input())):
    n = int(input())
    words = input().split()
    i, j = [word for word in words if len(word) == n-1]
    result = 'YES' if i == j[::-1] else 'NO'
    print(result)
