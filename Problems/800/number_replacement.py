##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Number Replacement                                       #
# Link: https://codeforces.com/contest/1744/submission/209894588 #
# Contest: 1744                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 61 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# To know if a string 'S' is obtainable from a list 'a' of integers
# we need to check if for each integer in 'a', his corresponding character
# in 'S' is also in each integer that contains the same numerical value.
#
# e.g:
#
# 1|2|2|1
# a|a|a|b
#
# In this case it's not obtainable, because the integer '1' and his
# character 'a', is not in each integer '1'. (1 = b)

# Implementation
def is_obtainable(s: str, a: list) -> bool:
    for idx1, digit in enumerate(a):
        for idx2, ch in enumerate(s):
            if digit == a[idx2] and s[idx1] != ch:
                return False
    return True

# Program running
for t_case in range(int(input())):
    n = input()
    a = list(map(int, input().split()))
    s = input()
    
    result = 'YES' if is_obtainable(s, a) else 'NO'
    print(result)
