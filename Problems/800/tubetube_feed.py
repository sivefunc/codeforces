##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: TubeTube Feed                                            #
# Link: https://codeforces.com/contest/1822/submission/206116364 #
# Contest: 1822                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 61 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# go to each video, save it to a watch list if you have time to watch
# the video, else skip to the next video
# each time u skip a video, your lunch time decrease by one

# Implementation
def corresponding_video(t, a, b):
    watch = []
    for idx, video in enumerate(a):
        if t == 0: # you don't have time to watch more videos :(
            return -1 if not watch else sorted(watch).pop()[1]

        elif video <= t: # you have time to watch that video
            t -= 1 # skip to the next video (it cost u 1 second)
            watch.append((b[idx], idx + 1)) # ENT value and idx of Video.

        else: # skip to the next video (it cost u 1 second)
            t -= 1

    return -1 if not watch else sorted(watch).pop()[1] # Video with more ENT
   
# Run of the program
for t_case in range(int(input())):
    n, t = map(int, input().split()) # Videos, time to spend on lunch
    a = map(int, input().split()) # Duration of videos
    b = [int(k) for k in input().split()] # Entertainment value of video
    print(corresponding_video(t, a, b))
