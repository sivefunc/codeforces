##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Codeforces Checking                                      #
# Link: https://codeforces.com/contest/1791/submission/208069280 #
# Contest: 1791                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 30 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# nothing to explain why is this a codeforces problem?
for t_case in range(int(input())):
    print('YES' if input() in 'codeforces' else 'NO')
