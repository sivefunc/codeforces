##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Word                                                     #
# Link: https://codeforces.com/contest/59/submission/211855632   #
# Contest: 59                                                    #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

s = input()
r = s.lower() if sum(i.islower() for i in s) >= int(len(s)/2+.5) else s.upper()
print(r)
