##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Unit Array                                               #
# Link: https://codeforces.com/contest/1834/submission/210572874 #
# Contest: 1834                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# To find the minimum number of operations that satisfy both conditions:
#
# a1 + a2 + a3 + ... + an >= 0
# a1 * a2 * a3 * ... * an = 1
#
# We need first to change all the '-1' to '1' until the sum we get is equal
# to 0, if the array is odd then we need to flip another '-1'.
# (sum will be == 1)
#
# Once we flip all the signs, we need now to satisfy the second condition.
#
# To do that we need to check if the -1's in the new array (the ones that
# were not flipped over 1) are odd or even.
#
# If the -1's are odd it means that the total product is -1, so we need to flip
# another -1 (Because odd multiplication of negative numbers is
# always negative)
#
# If the -1's are even it means that the total product is 1, so we don't need
# to flip, the condition is already being satisfied. (Because even
# multiplication of negative numbers is always positive)

# Program running
for t_case in range(int(input())):
    n = int(input())
    a = list(map(int, input().split()))

    n_ns = a.count(-1)
    p_ns = a.count(1)

    if p_ns >= n_ns:
        result = 0 if n_ns % 2 == 0 else 1

    else:
        distance = int(n / 2 + 0.5) - p_ns
        result = distance + (0 if (n_ns - distance) % 2 == 0 else 1)

    print(result)
