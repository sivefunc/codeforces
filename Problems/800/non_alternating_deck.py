##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: A1. Non-alternating Deck (easy version)                  #
# Link: https://codeforces.com/contest/1786/submission/216763249 #
# Contest: 1786                                                  #
# Problem: A1                                                    #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 77 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    n = int(input())

    if n <= 6:
        alice = 1
        bob = n-1

    else:
        n -= 6
        alice = 1
        bob = 5

        a_last = 1
        b_last = 3

        while n:
            a_cards = a_last*2 + 7
            b_cards = b_last*2 + 7

            alice += n if a_cards >= n else a_cards
            n -= n if a_cards >= n else a_cards

            if n:
                bob += n if b_cards >= n and n else b_cards
                n -= n if b_cards >= n and n else b_cards
    
            a_last += 4
            b_last += 4

    print(alice, bob)
