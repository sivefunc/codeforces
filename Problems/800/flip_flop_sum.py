##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Flip Flop Sum                                            #
# Link: https://codeforces.com/contest/1778/submission/208362429 #
# Contest: 1778                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 62 ms                                                    #
# Memory: 4748 KB                                                #
##################################################################

# Explanation
#
# According to the operation restraint, there are 3 outcomes:
#
# A) Flip (-1 and -1)
# B) Flip (-1 and 1) or (1 and -1)
# C) Flip (1 and 1)
#
# If we want the maximum sum we need to go from operation A to B and last C.
#
# Operation A, will give u the max sum because flipping two negative numbers
# will give two positive numbers (positive means bigger in math context)
#
# Operation B, will give u the second max sum because flipping these numbers
# will not change to a bigger or smaller number.
#
# Operation C, will give u the smallest sum, because flipping two positive
# numbers will give two negative numbers (negative means smaller in math
# context)
#
# If you can't do operation A go to op B else go to C else go to mars.

# Program running
for t_case in range(int(input())):
    n = input()
    a = input().split()
    b = ''.join(a) # Integers in string form
    s = sum(int(i) for i in a)

    # One liner ._.
    result = s + 4 if '-1-1' in b else s if '-11' in b or '1-1' in b else s - 4
    print(result)
