##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Marathon                                                 #
# Link: https://codeforces.com/contest/1692/submission/220745550 #
# Contest: 1692                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 202 ms                                                   #
# Memory: 6888 KB                                                #
##################################################################

for t_case in range(int(input())):
    a, b, c, d = map(int, input().split())
    result = 4 - (sorted([a,b,c,d]).index(a) + 1)
    print(result)
