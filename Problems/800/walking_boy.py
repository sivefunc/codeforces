##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Walking Boy                                              #
# Link: https://codeforces.com/contest/1776/submission/219570572 #
# Contest: 1776                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 61 ms                                                    #
# Memory: 1764 KB                                                #
##################################################################

for t_case in range(int(input())):
    n = input()
    a = [0] + list(map(int, input().split())) + [1440]

    walks = 0
    for idx, i in enumerate(a[:-1]):
        if a[idx+1] - i >= 240:
            walks += 2
        
        elif a[idx+1] - i >= 120:
            walks += 1

    print('YES' if walks >= 2 else 'NO')
