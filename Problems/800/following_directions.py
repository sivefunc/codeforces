##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Codeforces Checking                                      #
# Link: https://codeforces.com/contest/1791/submission/208068744 #
# Contest: 1791                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
# 
# Start from 0,0 and move according to the direction they gave u
# If the coordinate X, Y is in the given point (1, 1) just return True

# Program running
for t_case in range(int(input())):
    input()
    X, Y = 0, 0
    result = 'NO'
    for move in input():
        if move in {'L', 'R'}:
            X = X - 1 if move == 'L' else X + 1

        else:
            Y = Y - 1 if move == 'D' else Y + 1

        if (X, Y) == (1, 1):
            result = 'YES'
            break

    print(result)
