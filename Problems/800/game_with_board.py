##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Game with Board                                          #
# Link: https://codeforces.com/contest/1841/submission/210772520 #
# Contest: 1841                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# We need to force Alice to win, if that's not possible it means that Bob wins.
#
# How we do that?
# Well, alice needs to wipe n-2 ones, so that when u do that bob will wipe
# the two ones left and alice will win, because there are different elements.
#
# This situation of forcing alice to win, is not possible for n < 5, because
# wiping n-2 ones, will make bob the winner because he can't add digits
# or the sum if n-2 ones will be equal to 2. 
#
# Look at this:
#
# 1,1 -> 2 -> Bob wins
# 1,1,1 -> 2,1 -> Bob wins
# 1,1,1,1 -> 3,1 -> Bob wins or -> 2,1,1 -> 2, 2 -> 4 Bob wins
#
# So the conclusion, is that if n < 5 bob wins, else alice wins.

for t_case in range(int(input())):
    n = int(input())
    result = 'Alice' if n >= 5 else 'Bob'
    print(result)
