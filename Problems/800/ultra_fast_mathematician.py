##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Ultra-Fast Mathematician                                 #
# Link: https://codeforces.com/contest/61/submission/213825602   #
# Contest: 61                                                    #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

s1 = input()
s2 = input()

result = ''.join("1" if i != j else '0' for i, j in zip(s1, s2))
print(result)
