##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Matching                                                 #
# Link: https://codeforces.com/contest/1821/submission/214977226 #
# Contest: 1821                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 155 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    s = input()
    r = 0 if s[0] == '0' else 1 if '?' not in s else \
            (9 if s[0] == '?' else 10) * 10**(s.count('?')-1)
    print(r)
