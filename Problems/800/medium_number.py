##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Medium Number                                            #
# Link: https://codeforces.com/contest/1760/submission/208641813 #
# Contest: 1760                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 77 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# why is this a problem?
for t_case in range(int(input())):
    print(sorted(int(i) for i in input().split())[1])
