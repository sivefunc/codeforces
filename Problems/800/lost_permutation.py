##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Lost Permutation                                         #
# Link: https://codeforces.com/contest/1759/submission/218710872 #
# Contest: 1759                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 77 ms                                                    #
# Memory: 3864 KB                                                #
##################################################################

for t_case in range(int(input())):
    m, s = map(int, input().split())
    b = list(map(int, input().split()))
    
    result = 'NO'
    k = 0
    for i in range(1, s+1):
        if i not in b:
            k += i
            b.append(i)
            if k > s:
                break

            elif k == s:
                result = 'YES' if sorted(b)==list(range(1, max(b)+1)) else 'NO'
                break

    print(result)
