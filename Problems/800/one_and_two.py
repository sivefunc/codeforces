##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: One and Two                                              #
# Link: https://codeforces.com/contest/1788/submission/206397640 #
# Contest: 1788                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 77 ms                                                    #
# Memory: 500 KB                                                 #
##################################################################

# Explanation
#
# The array consists of the numbers 1 and 2, and because we are looking
# for a equality in k we need a even number of 2s (because the 1s in the array
# doesnt have an effect in the product)
#
# If the array consists of only 1s, it means that k=1 (product is the same)
# If the array consists of odd number of 2s, it means that k = -1, e.g:
# 2,2,2,2,2 -> you can't get an equality, because 2*2 != 2*2*2

# Implementation
def k_exists(integers: list,  q2s_in_total) -> int:
    if q2s_in_total % 2 != 0:
        return -1

    k = 0
    q2s_you_need = q2s_in_total // 2
    q2s_you_have = 0

    while q2s_you_need != q2s_you_have:
        if integers[k] == 2:
            q2s_you_have += 1
        k += 1

    return k

# Program running
for t_case in range(int(input())):
    input()
    n = [int(k) for k in input().split()]
    result = 1 if not n.count(2) else k_exists(n, n.count(2))
    print(result)
