##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Make A Equal to B                                        #
# Link: https://codeforces.com/contest/1736/submission/219817090 #
# Contest: 1736                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 77 ms                                                    #
# Memory: 2868 KB                                                #
##################################################################

for t_case in range(int(input())):
    n = int(input())
    a = list(map(int, input().split()))
    b = list(map(int, input().split()))
    
    x = sum(i!=j for i, j in zip(a,b))
    y = 1 + abs(a.count(1)-b.count(1))
    result = x if x < y else y
    print(result)
