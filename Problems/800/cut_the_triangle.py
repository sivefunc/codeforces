##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Cut the Triangle                                         #
# Link: https://codeforces.com/contest/1767/submission/215439770 #
# Contest: 1767                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 155 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

# This is another solution, this was made after I solved the problem
# https://codeforces.com/contest/1767/submission/215441224

# This is my working solution before reading the editorial
# https://codeforces.com/contest/1767/submission/215439770

# To cut the triangle into two, the type of triangle can't be right triangle
# So, we need to demonstrate that the triangle is not a right triangle.
#
# There are different ways to prove that a triangle is right or not, one way
# is by:
#
# Checking if two vertex has the same X coordinate and one of them
# Have the same Y coordinate but with a different Vertex.

import itertools

for t_case in range(int(input())):
    input()
    rotations = list(itertools.permutations([
    list(map(int, input().split())) for i in range(3)]))

    result = 'YES'
    for coords in rotations:
        (Ax, Ay), (Bx, By), (Cx, Cy) = coords
        if Ax == Bx and By == Cy or Ay == By and Bx == Cx:
            result = 'NO'
            break

    print(result)
