##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Find and Replace                                         #
# Link: https://codeforces.com/contest/1807/submission/210139819 #
# Contest: 1807                                                  #
# Problem: C                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 217 ms                                                   #
# Memory: 184 KB                                                 #
##################################################################

# Explanation
#
# To know if we can convert it to an alternating string, we need to check
# if each occurence of a character 'i' is located in a even or odd only index.
#
# We do this, because the quantity of characters between the characters 'i'
# needs to be odd, to allow forming a binary string, without adjacent bits.
#
# E.g
#
# |a|x|x|a|
# |1|2|3|4|
#
# 'axxa', you can't convert it to an alternating string because the character
# 'a' is located in a odd index but also in a even index, so the quantity
# of characters between them is an even number, and it's impossible to add bits
# that are not adjacent in that situation.

# Implementation
def is_alternating(s: str) -> bool:
    individual_characters = set(s)
    for ch1 in individual_characters:
        idx1 = s.index(ch1)
        for idx2, ch2 in enumerate(s):
            if ch1 == ch2 and (idx1 % 2 == 0 and idx2 % 2 != 0 or 
                                idx1 % 2 != 0 and idx2 % 2 == 0):
                return False
    
    return True

# Program running
for t_case in range(int(input())):
    input()
    s = input()
    result = 'YES' if is_alternating(s) else 'NO'
    print(result)
