##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: GamingForces                                             #
# Link: https://codeforces.com/contest/1792/submission/210269607 #
# Contest: 1792                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 108 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Using the first spell is only worth it for monsters with 1 health
# The second spell is only worth it for monsters with health > 1
#
# This is because with the first spell, you are killing two monters
# at 1 spell cast, but if they have a greater health let's say 9
# It will take you 9 spell casts to kill them using the first spell.
# 
# instead, you have to use the second spell and kill the two monsters
# at 2 spell casts.
#
# So at the end, if u want the minimum amount of spell casts, start using
# the 1st spell for monsters with 1 hp, then if there are no more start killing
# monsters with hp > 1 using the 2nd spell.

# Program running
for t_case in range(int(input())):
    monsters_quantity = int(input())
    monsters_healths = list(map(int, input().split()))
    m_1hp = monsters_healths.count(1)
    m_more_1hp = monsters_quantity - m_1hp

    result = int(m_1hp / 2 + 0.5) + m_more_1hp
    print(result)
