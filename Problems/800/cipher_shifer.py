##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Cipher Shifer                                            #
# Link: https://codeforces.com/contest/1840/submission/208760663 #
# Contest: 1840                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

for t_case in range(int(input())):
    input()
    s = input() + ' '
    a = ''
   
    ch1 = ''
    for idx, ch2 in enumerate(s[:-1]):
        if idx == 0 or ch1 == ch2:
            a += ch2
            ch1 = ''

        else:
            if ch2 == a[-1]:
                ch1 = s[idx+1]

    print(a)
