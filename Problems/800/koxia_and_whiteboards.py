##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Koxia and Whiteboards                                    #
# Link: https://codeforces.com/contest/1770/submission/221086149 #
# Contest: 1770                                                  #
# Problem: A                                                     #
# Rating: 1000                                                   #
# Status: Solved                                                 #
# Time: 233 ms                                                   #
# Memory: 9096 KB                                                #
##################################################################

for t_case in range(int(input())):
    n, m = map(int, input().split())
    a = list(map(int, input().split()))
    b = list(map(int, input().split()))

    for i in b:
        a.remove(min(a))
        a.append(i)

    print(sum(a))
