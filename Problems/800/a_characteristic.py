##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: A-Characteristic                                         #
# Link: https://codeforces.com/contest/1823/submission/211060731 #
# Contest: 1823                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Create the array with the highest A-Characteristic, that array
# is equal to an array of lenght n filled with only "1's" or only "-1's", then
# check if his A-Characteristic is equal to 'k', if it's not equal then
# start modifying the array by changing the first element to his opposite sign.
# then check, if it's not equal change the next element to the opposite sign,
# keep doing this process until you get the A-Characteristic being
# equal to 'k'.
#
# Note 1: If you flipped all the elements, it means that such A-Characteristic
# being equal to'k' doesn't exist.
# 
# ----------------------------------------------------------------------------
# To find the A-Characteristic of an array look at this pattern:
# (Do the drawings of each array, and it will be easier to comprehend it)
#
# 1, 1 = 1
# 1, 1, 1 = 3
# 1, 1, 1, 1 = 6
# 1, 1, 1, 1, 1, = 10
# 1, 1, 1, 1, 1, 1 = 15
#
# The A-characteristic of an array is equal to: 
# (The Lenght of array - 1) + A-Ch of the past array (Array that got rmd a 1)
#
# So you can define this as a sum or as a recursive function.
# We stop until we get to the initial array [1, 1]
#
# If the array contains flipped signs, you need to separate the array into
# two different arrays, one with positive elements and the another one with
# negative elements, and find the a-characteristic of each array (and sum it)

# Implementation
def a_characteristic(s: list) -> int:
    return sum(range(s.count(1)-1, 0, -1)) + sum(range(s.count(-1)-1, 0, -1))

# Program  running
for t_case in range(int(input())):
    n, k = map(int, input().split())
    array = [1] * n

    while (characteristic := a_characteristic(array)) != k:
        array = array[1:] + [-1]
        if array == [-1] * n:
            break
    
    r = 'YES\n' + ' '.join(map(str, array)) if characteristic == k else 'NO'
    print(r)
