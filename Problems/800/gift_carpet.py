##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Gift Carpet                                              #
# Link: https://codeforces.com/contest/1862/submission/220185567 #
# Contest: 1862                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 140 ms                                                   #
# Memory: 5332 KB                                                #
##################################################################

for t_case in range(int(input())):
    n, m = map(int, input().split())
    carpet = [input() for i in range(n)]
    letters = 'vika'
    letter = 'v'

    result = 'NO'
    for column in range(m):
        if letter in ''.join(row[column] for row in carpet):
            if letter == 'a':
                result = 'YES'
                break
            
            else:
                letter = letters[letters.index(letter)+1]

    print(result)
