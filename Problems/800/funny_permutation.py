##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Funny Permutation                                        #
# Link: https://codeforces.com/contest/1741/submission/209006162 #
# Contest: 1741                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 202 ms                                                   #
# Memory: 10420 KB                                               #
##################################################################

# Explanation
#
# According to the problem, you only need one neighbor that is
# strictly less or more than his neighbor, so making a decreasing or increasing
# sequence will work:
# 7, 6, 5, 4, 3, 2, 1 or 1, 2, 3, 4, 5, 6, 7 but there is one more restriction
# The index of the number can't be equal to the number itself.
#
# For these examples the number 4 and 1 is equal to his index.
#
# Shifting the numbers in a decreasing or increasing seq. where the index is
# equal to the number won't solve that.
#
# The idea to solve the problem:
# We need to remove a pair of numbers that shifts all the values to an index
# that's not equal to the number, we add these pair of numbers to a given
# position  in the sequence
#
# For simplicity that numbers will be 1 and 2 and those will give added to
# the end of the sequence.
#
# 3, 4, 5, 6, 7, ... , 1, 2
# As you can see the condition is satisfied. for any n >= 4.
# Any pair of numbers as far as I know it could work.
# Obviously you need to know where to put the pair of numbers.
# The ideal condition as far as I know is in the end and in the beginning.
#
# 7, 6, 1, 2, 3, 4, 5.
# 4 3 1 2
#
# Obviously this won't work if you add these numbers at the end.
#
# If n == 3, it can't be done.
#
# Note: We are removing and adding in adjacent pairs, because it will work
# as a support for the other number to satisfy the condition if u add it
# in a random place. so you can't add random numbers as a pair.

# Program running
for t_case in range(int(input())):
    n = int(input())

    result = [i+3 for i in range(n-2)] + [2, 1] if n != 3 else [-1]
    print(*result) # using this instead of map(str, list) or join, very useful
