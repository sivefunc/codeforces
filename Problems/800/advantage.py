##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Advantage                                                #
# Link: https://codeforces.com/contest/1760/submission/209210092 #
# Contest: 1760                                                  #
# Problem: C                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 343 ms                                                   #
# Memory: 26376 KB                                               #
##################################################################

# Explanation
#
# We need to find the two strongest participants and then compare
# the strongest participant to each participant, if the participant
# is the strongest participant we compare it with the second strongest.
#
# Because according to problem rules, the participant can't compare to itself.

# Old solution, before reading editorial
# https://codeforces.com/contest/1760/submission/209209194
# differences = []
# for integer in integers:
#   for idx, max_int in enumerate(max_integers):
#       if max_int == integer:
#           if max_integers[idx+1] == integer:
#                   differences.append(0)
#                   break
#               continue
#            
#            differences.append(integer - max_int)
#            break

# Program running
for t_case in range(int(input())):
    input()
    integers = [int(i) for i in input().split()]
    max1, max2 = sorted(integers, reverse=True)[:2]

    # New solution, how didn't i see this?
    differences = [n - max1 if n != max1 else n - max2 for n in integers]
    print(*differences)
