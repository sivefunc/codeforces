##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Blank Space                                              #
# Link: https://codeforces.com/contest/1829/submission/206281074 #
# Contest: 1829                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Save all the blank spaces into a list and sort it, the last item will be
# the longest (the sorted function that's how it sorted it)
#
# To find the blank spaces we just split the string using 1 as a separator
# Because a blank space will always be next to a 1, or between two 1s, eg:
#
# 01000010 -> 0, 0000, 0
# 00000010 -> 00000, 0
#
# If 1 wasn't there the blank space would keep increasing up to len of string.

# Program running
for t_case in range(int(input())):
    n = input()
    array = ''.join(input().split()).split('1')
    longest_blank_space = sorted(array)[-1]
    print(len(longest_blank_space))
