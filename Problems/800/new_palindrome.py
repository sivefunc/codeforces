##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: New Palindrome                                           #
# Link: https://codeforces.com/contest/1832/submission/208839769 #
# Contest: 1832                                                  #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 31 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# Divide the string into two parts, it's possible because string is a
# palindrome, into the two parts you don't include the middle (in the
# case that string is odd)
#
# So if you find a subsequence of 2 different characters in first part
# that is also located in second part it means that you can interchange
# these characters.
#
# Optimization:
# The second part of the string is the mirror of the first part of string
# So you don't have to check the second string, just the first.
#
# Instead of examinating each character in first part, just use set()
# if the len is > 1 it means that you can interchange characters.

def can_get(palindrome: str) -> str:
    half = palindrome[:len(palindrome)//2]
    for idx, ch in enumerate(half[:-1]):
        if ch != half[idx+1]:
            return 'YES'
    return 'NO'

# Testing this new way of delivering multi line input
results = [can_get(input()) for t_case in range(int(input()))]
print('\n'.join(map(str, results)))
