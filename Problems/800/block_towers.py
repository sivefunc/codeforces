##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Block Towers                                             #
# Link: https://codeforces.com/contest/1767/submission/209305530 #
# Contest: 1767                                                  #
# Problem: B                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 186 ms                                                   #
# Memory: 25868 KB                                               #
##################################################################

# Explanation
#
# First we need to sort the towers from the lowest to the highest tower.
# Except the first tower (we take the tower out), we do this to maximize
# The numbers of blocks that will be in the first tower.
# Because if you took the max tower and add the blocks to the first tower.
# Probably you will find that the first tower is higher than most towers.
#
# After you sort it, you start moving the blocks between the second tower
# to the first tower, this will increase T1 to +1 and T2 to -1.
#
# That's it, the problem is that it's too slow doing these operations.
# https://codeforces.com/contest/1767/submission/207279485
#
# So we need to reduce the operations of adding and subtracting to a formula.
# Let's look at these three patterns between two towers.
#  ______________________________________________________________
# |T1 | T2  ------------------ T1 | T2 ------------------ T1 | T2|
# |8  | 15  ------------------ 8  | 16 ------------------ 8  | 17|
# |--------------------------------------------------------------|
# |9  | 14  ------------------ 9  | 15 ------------------ 9  | 16|
# |10 | 13  ------------------ 10 | 14 ------------------ 10 | 15|
# |11 | 12  ------------------ 11 | 13 ------------------ 11 | 14|
# |12 | 11  ------------------ 12 | 12 ------------------ 12 | 13|
# |                                                       13 | 12|
# |--------------------------------------------------------------|
# 
# If you look T1 = 8, T2 = 16
#
# A) The total of adding and subtracting blocks in the towers, depends in the
# distance between T1, T2, because if you see T1 it got added +4
# And T2 got subtracted -4, the total would be 8 (absolute value)
#
# B) Now but we wan't to find how many blocks T1 got added, not the total.
# If we look at A) you will see that it got added the absolute value / 2 
# Because the operation of adding and substracting are in pairs.
# 
# Now, another thing to notice, if the distance is odd you need to add 1
# to T1.

# Program running
for t_case in range(int(input())):
    input()
    towers = [int(i) for i in input().split()]
    
    first_tower = towers.pop(0)
    for i_tower in sorted(towers):
        if first_tower < i_tower:
            first_tower += int((i_tower - first_tower) / 2 + 0.5)
    
    print(first_tower)
