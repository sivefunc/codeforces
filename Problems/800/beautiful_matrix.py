##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Beautiful Matrix                                         #
# Link: https://codeforces.com/contest/263/submission/211541554  #
# Contest: 263                                                   #
# Problem: A                                                     #
# Rating: 800                                                    #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

matrix = [input() for i in range(5)]
row_with_1 = [i for i in matrix if '1' in i][0]

Y_moves = abs(2 - matrix.index(row_with_1))
X_moves = abs(2 - row_with_1.split().index('1'))

print(Y_moves + X_moves)
