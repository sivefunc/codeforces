##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Maximum Number                                           #
# Link: https://codeforces.com/contest/774/submission/203730335  #
# Contest: 774                                                   #
# Problem: C                                                     #
# Rating: 1200                                                   #
# Status: Solved                                                 #
# Time: 265 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# 2 Sections highlighted = Digit 1
# 3 Sections highlighted = Digit 7
# 4 Sections highlighted = Digit 4
# 5 Sections highlighted = Digits 2, 3 and 5 -> max digit here is 5
# 6 Sections highlighted = Digits 0, 6 and 9 -> max digit here is 9
# 7 Sections highlighted = Digit 8

# After decomposition, with digit 1 you can maximize the n of sections
# Max number of even sections 2, 4, 6, 8, 10 ... n is eq to n / 2
# Max number of odd sections 3, 5, 7, 9 ... n is eq to the max number of
# even sections but replacing first number to 7.

n = int(input()) # Number of sections which can be highlighted
max_int = (n // 2) * '1' if n % 2 == 0 else '7' + (n // 2 - 1) * '1'
print(int(max_int))
