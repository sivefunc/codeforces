##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: IQ test                                                  #
# Link: https://codeforces.com/contest/25/submission/217036808   #
# Contest: 25                                                    #
# Problem: A                                                     #
# Rating: 1300                                                   #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

n = int(input())
a = list(map(int, input().split()))
b = [i%2 for i in a]

print(b.index(0)+1 if b.count(0) == 1 else b.index(1)+1)
