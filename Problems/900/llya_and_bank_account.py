##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Ilya and Bank Account                                    #
# Link: https://codeforces.com/contest/313/submission/221973141  #
# Contest: 313                                                   #
# Problem: A                                                     #
# Rating: 900                                                    #
# Status: Solved                                                 #
# Time: 124 ms                                                   #
# Memory: 0 KB                                                   #
##################################################################

n = input()

print(max(int(n), int(n[:-1]), int(n[:-2] + n[-1])))
