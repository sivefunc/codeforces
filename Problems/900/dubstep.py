##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Dubstep                                                  #
# Link: https://codeforces.com/contest/208/submission/213964353  #
# Contest: 208                                                   #
# Problem: A                                                     #
# Rating: 900                                                    #
# Status: Solved                                                 #
# Time: 92 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

print(' '.join(filter(lambda x: x != '', input().split('WUB'))))
