##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Hits Different                                           #
# Link: https://codeforces.com/contest/1829/submission/205262856 #
# Contest: 1829                                                  #
# Problem: G                                                     #
# Rating: 1600                                                   #
# Status: Unsolved                                               #
# Time: 2500+ ms                                                 #
# Memory: 81212 KB                                               #
##################################################################

# Implementation
def pyramid(rows: int) -> list:
    """
    return a 2d list containing a pyramid justified to the left
    """

    p = []
    p_row = []
    n = 1
    
    for row in range(1, rows + 1):
        # Start from 1 because each row contains the lenght of itself
        for column in range(row):
            p_row.append(n)
            n += 1 # Counter
        p.append(p_row)
        p_row = []

    return p

def find_n(pyramid: list, n: int) -> tuple:
    """
    return a tuple containing the row and column coordinate of the n can
    """

    for idx, row in enumerate(pyramid):
        if n in row:
            return idx, row.index(n)

def stacked_cans(pyramid: list, can: tuple) -> set:
    """
    return a set containing the cans that are stacked in n can
    incluiding itself
    """

    can_row, can_column = can

    # Sets to remove duplicates, if you look at the graph you will see duplicates.
    cans_stacked = {pyramid[can_row][can_column]}
    cans_to_check = {can}
    future_check = set()

    while cans_to_check:
        for can in cans_to_check:
            can_row, can_column = can
            
            # Left can, in this case is just a diagonal
            if can_row - 1 >= 0 and can_column - 1 >= 0:
                future_check.add((can_row - 1, can_column - 1))
                cans_stacked.add(pyramid[can_row - 1][can_column - 1])

            # Right can
            # Left justified triangle just needs to decrease by -1 the row
            # The column doesnt increase by 1, it keeps the same, so it goes up
            # It always go up if the column is located in can_row - 1
            if can_row - 1 >= 0 and can_row > can_column:
                future_check.add((can_row - 1, can_column))
                cans_stacked.add(pyramid[can_row - 1][can_column])
        
        cans_to_check = future_check
        future_check = set()
        
    return cans_stacked

# Program running
for t in range(int(input())):
    can = int(input())
    cans = stacked_cans(p, find_n(p, can))
    print(sum(cans))
