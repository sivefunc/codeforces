##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Knights                                                  #
# Link: https://codeforces.com/contest/1221/submission/205925881 #
# Contest: 1221                                                  #
# Problem: B                                                     #
# Rating: 1100                                                   #
# Status: Solved                                                 #
# Time: 46 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
#
# After playing with the pieces in lichess i found out that:
# black pieces will be in white squares and
# white pieces will be in black squares.
#
# It's almost identical to the matrix that codeforces gave as 3x3.
#
# This pattern works because a knight in a square of color x always attacks
# the inverse of that color x, If the knight is in white square it will attack
# the black squares, so if we are looking to maximize the number of duels
# in a chess board, you have to follow that pattern.

n = int(input()) # Size of nxn matrix
bw = 'BW' * (n//2) + ('' if n % 2 == 0 else 'B')
wb = 'WB' * (n//2) + ('' if n % 2 == 0 else 'W')
 
for row in range(1, n+1): print(bw if row % 2 == 0 else wb)
