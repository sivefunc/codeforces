##################################################################
# Author information ->                                          #
# AKA: Sivefunc                                                  #
# Codeforces account: https://codeforces.com/profile/sivefunc    #
# Gitlab account: https://gitlab.com/sivefunc                    #
# Codeberg account: https://codeberg.org/Sivefunc                #
#                                                                #
# Problem information ->                                         #
# Name: Home Numbers                                             #
# Link: https://codeforces.com/contest/638/submission/203600661  #
# Contest: 638                                                   #
# Problem: A                                                     #
# Rating: 1100                                                   #
# Status: Solved                                                 #
# Time: 93 ms                                                    #
# Memory: 0 KB                                                   #
##################################################################

# Explanation
# Distance between two points divided into sections of 2 elements (house and S)
# In this case every section represent a second, because it's the time that
# the car takes to travel between two houses of distance 1.

n, a = map(int, input().split()) # Number of houses and location of a house
seconds = -(1 - a) / 2 + 1 if a % 2 != 0 else (n - a) / 2 + 1
print(int(seconds)) # We could also use //, output must be int
