# Codeforces - Documentation of own solutions to problems

Codeforces is a competitive programming website with a lot of interesting problems that has been working for over 10 years, here's my own solutions to some of the problems I found, hopefully the readers of my code will understand without problems how it was solved.

![Logo](Images/logo.svg)

## Made by [Sivefunc](https://gitlab.com/sivefunc)
## Licensed under [GPLv3](LICENSE)
